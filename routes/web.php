<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/roles', 'PermissionController@Permission');

Route::group(['middleware' => 'role:manager'], function() {

	Route::get('/admin', function() {

		return 'Welcome Admin';

	});

});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','MainPageController@mainPage');
Route::get('about','MainPageController@aboutPage');
Route::get('product','MainPageController@productPage');
Route::get('product-details/{id}','MainPageController@productDetailPage');
Route::get('custom-design','MainPageController@customDesignPage');
Route::get('cart','MainPageController@cartPage');
Route::get('checkout','MainPageController@checkoutPage');
Route::get('privacy','MainPageController@privacyPage');
Route::get('faq','MainPageController@faqPage');
Route::get('blogs','MainPageController@blogsPage');
Route::get('blog-details','MainPageController@blogDetailPage');
Route::get('contact','MainPageController@contactPage');
Route::get('/variation-search','MainPageController@variationSearch');
Route::get('/search-category','MainPageController@CategorySearch');

Route::group(['Auth'], function() {
	Route::prefix('home')->group(function () {
		Route::get('users','AllUsersController@index');
		Route::get('user/add','AllUsersController@create');
		Route::post('user/add','AllUsersController@store');
		Route::get('user/edit/{id}','AllUsersController@edit');
		Route::post('user/edit/{id}','AllUsersController@update');
		Route::delete('user/delete/{id}','AllUsersController@destroy');

		Route::get('auth-user/edit/{id}','AllUsersController@userEdit');

		Route::get('roles','RolesController@index');
		Route::get('role/create','RolesController@create');
		Route::post('role/create','RolesController@store');
		Route::get('role/edit/{id}','RolesController@edit');
		Route::post('role/edit/{id}','RolesController@update');
		Route::delete('role/delete/{id}','RolesController@destroy');

		Route::get('product-category','ProductCategoryController@index');
		Route::get('product-category/create','ProductCategoryController@create');
		Route::post('product-category/create','ProductCategoryController@store');
		Route::get('product-category/edit/{id}','ProductCategoryController@edit');
		Route::post('product-category/edit/{id}','ProductCategoryController@update');
		Route::delete('product-category/delete/{id}','ProductCategoryController@destroy');

		Route::get('products','ProductController@index');
		Route::get('product/create','ProductController@create');
		Route::post('product/create','ProductController@store');
		Route::get('product/edit/{id}','ProductController@edit');
		Route::post('product/edit/{id}','ProductController@update');
		Route::delete('product/delete/{id}','ProductController@destroy');

		Route::get('/blog-category', 'BlogcategoryController@index');
		Route::get('/blog-category/create', 'BlogcategoryController@create');
		Route::post('/blog-category/create', 'BlogcategoryController@store')->name('make.blog');
		Route::get('/blog-category/{id}', 'BlogcategoryController@show');
		Route::get('/blog-category/edit/{id}', 'BlogcategoryController@edit');
		Route::post('/blog-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
		Route::get('/blog-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');

		Route::get('/blogs', 'BlogController@index');
		Route::get('/blog/create', 'BlogController@create');
		Route::post('/blog/create', 'BlogController@store')->name('make-post.blog');
		Route::get('/blog/{id}', 'BlogController@show');
		Route::get('/blog/edit/{id}', 'BlogController@edit');
		Route::post('/blog/edit/{id}', 'BlogController@update')->name('update-post.blog');
		Route::get('/blog/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');

		Route::get('tasks','TaskController@index');
		Route::get('task/create','TaskController@create');
		Route::post('task/create','TaskController@store');
		Route::get('task/edit/{id}','TaskController@edit');
		Route::post('task/edit/{id}','TaskController@update');
		Route::delete('task/delete/{id}','TaskController@destroy');

		Route::get('product-type','ProductTypeController@index');
		Route::get('product-type/create','ProductTypeController@create');
		Route::post('product-type/create','ProductTypeController@store');
		Route::get('product-type/edit/{id}','ProductTypeController@edit');
		Route::post('product-type/edit/{id}','ProductTypeController@update');
		Route::delete('product-type/delete/{id}','ProductTypeController@destroy');
	});
});





Route::fallback(function(){
	return view ('frontend.nopage');
});