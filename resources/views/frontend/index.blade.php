@extends('layouts.frontend.app')

@section('content')

    <!-- hero-section start -->
    <section class="hero-section">
      <div class="hero-woman wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"><img src="assets/images/hero-left-woman.png" alt="image"></div>
      <div class="hero-man wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"><img src="assets/images/hero-right-man.png" alt="image"></div>
      <div class="hero-circle-shape wow zoomIn" data-wow-duration="1s" data-wow-delay="1.5s"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-10">
            <div class="hero-content-area">
              <h2 class="hero-title text-center mb-50">The new standard for print-on-demand</h2>
              <div class="d-flex flex-wrap justify-content-md-between hero-item-area">
                <div class="hero-item">
                  <h3 class="title mb-15">choose your design form in here</h3>
                  <p>Lorem ipsum dolor sit amet, ettotaquet elit eu quisque leo primis eros mi diam elit praesenturna nulla </p>
                  <a href="#0" class="btn btn-secondary mt-30">shop now design</a>
                </div><!-- hero-item end -->
                <div class="hero-item">
                  <h3 class="title mb-15">create your design by your own idea</h3>
                  <p>Lorem ipsum dolor sit amet, ettotaquet elit eu quisque leo primis eros mi diam elit praesenturna nulla </p>
                  <a href="#0" class="btn btn-primary mt-30">shop now design</a>
                </div><!-- hero-item end -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- hero-section end -->

    <!-- brand-section start -->
    <div class="brand-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-3">
            <h2 class="brand-area-title">Who`s using Nepgeeks</h2>
          </div>
          <div class="col-lg-9">
            <div class="brand-slider">
              <div class="slide-item"><img src="assets/images/brand/1.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/2.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/3.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/4.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/5.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/6.png" alt="image"></div>
              <div class="slide-item"><img src="assets/images/brand/1.png" alt="image"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- brand-section end -->

    <!-- choose-us-section  start -->
    <section class="choose-us-section pt-150">
      <div class="el-1" data-paroller-factor="-0.3" data-paroller-type="foreground" data-paroller-direction="horizontal"><img src="assets/images/elements/t-shirt.png" alt="image"></div>
      <div class="el-2" data-paroller-factor="-0.1" data-paroller-type="foreground" data-paroller-direction="horizontal"><img src="assets/images/elements/bag.png" alt="image"></div>
      <div class="el-3" data-paroller-factor="-0.2" data-paroller-type="foreground" data-paroller-direction="horizontal"><img src="assets/images/elements/cup.png" alt="image"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-6">
            <div class="section-header text-xl-left text-center">
              <h2 class="section-title">Why choose Nepgeeks for print on demand</h2>
              <p>Placerat metus rhoncus cras netus veniam, sed odio. Id  vestivlus vehicula curabitur lorem pulvinar rutrum netus.solliudin cras vel velpuere urna eu nibh pellentesque.</p>
            </div>
            <div class="row mt-50 mb-none-40">
              <div class="col-sm-6">
                <div class="choose-item mb-40">
                  <div class="item-header">
                    <i class="flaticon-website"></i>
                    <h3 class="title">Free signup</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ribldit turpis erat, vivamus taciti laoreuuis donec cum mauris amet aliquet </p>
                </div>
              </div><!-- choose-item end -->
              <div class="col-sm-6">
                <div class="choose-item mb-40">
                  <div class="item-header">
                    <i class="flaticon-box"></i>
                    <h3 class="title">Low shipping</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ribldit turpis erat, vivamus taciti laoreuuis donec cum mauris amet aliquet </p>
                </div>
              </div><!-- choose-item end -->
              <div class="col-sm-6">
                <div class="choose-item mb-40">
                  <div class="item-header">
                    <i class="flaticon-order"></i>
                    <h3 class="title">Automated</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ribldit turpis erat, vivamus taciti laoreuuis donec cum mauris amet aliquet </p>
                </div>
              </div><!-- choose-item end -->
              <div class="col-sm-6">
                <div class="choose-item mb-40">
                  <div class="item-header">
                    <i class="flaticon-height"></i>
                    <h3 class="title">No minimums</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ribldit turpis erat, vivamus taciti laoreuuis donec cum mauris amet aliquet </p>
                </div>
              </div><!-- choose-item end -->
            </div>
          </div>
          <div class="col-xl-6">
            <div class="row choose-thumb-area mb-none-30">
              <div class="col-lg-12">
                <div class="choose-thumb mb-30"><img src="assets/images/choose/1.jpg" alt="image"></div>
              </div>
              <div class="col-sm-6">
                <div class="choose-thumb mb-30"><img src="assets/images/choose/2.jpg" alt="image"></div>
              </div>
              <div class="col-sm-6">
                <div class="choose-thumb mb-30"><img src="assets/images/choose/3.jpg" alt="image"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- choose-us-section  end -->

    <!-- how-work-section start -->
    <section class="how-work-section pt-150">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="video-thumb">
              <img src="assets/images/bg/video.jpg" alt="image">
              <a href="https://www.youtube.com/embed/GT6-H4BRyqQ" data-rel="lightcase:myCollection" class="video-icon"><i class="fa fa-play"></i></a>
            </div>
          </div>
        </div>
        <div class="row mb-none-30 pt-150 position-relative">
          <div class="item-line-1"><img src="assets/images/elements/line-1.png" alt="image"></div>
          <div class="item-line-2"><img src="assets/images/elements/line-2.png" alt="image"></div>
          <div class="col-lg-4 col-md-6 mb-30">
            <div class="how-work-item">
              <div class="icon-thumb">
                <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
                <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
                <div class="icon"><img src="assets/images/icon/how-work/1.png" alt="image"></div>
              </div>
              <div class="content mt-30">
                <h3 class="title mb-15">Choose product</h3>
                <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
              </div>
            </div>
          </div><!-- how-work-item end -->
          <div class="col-lg-4 col-md-6 mb-30">
            <div class="how-work-item">
              <div class="icon-thumb">
                <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
                <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
                <div class="icon"><img src="assets/images/icon/how-work/2.png" alt="image"></div>
              </div>
              <div class="content mt-30">
                <h3 class="title mb-15">Create design</h3>
                <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
              </div>
            </div>
          </div><!-- how-work-item end -->
          <div class="col-lg-4 col-md-6 mb-30">
            <div class="how-work-item">
              <div class="icon-thumb">
                <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
                <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
                <div class="icon"><img src="assets/images/icon/how-work/3.png" alt="image"></div>
              </div>
              <div class="content mt-30">
                <h3 class="title mb-15">Home delivery</h3>
                <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
              </div>
            </div>
          </div><!-- how-work-item end -->
        </div>
      </div>
    </section>
    <!-- how-work-section end -->
    <!-- product-section start -->
    <section class="product-section pt-150 pb-150">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="section-header text-center">
              <h2 class="section-title">Show your design to over <span>250+ products</span></h2>
              <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin </p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="product-slider-area">
              <div class="product-slider">
                <div class="product-single-slide">
                  <a href="#0" class="item-link"></a>
                  <div class="thumb"><img src="assets/images/product/1.png" alt="image"></div>
                  <div class="content">
                    <h3 class="product-name">Man t-shirt</h3>
                    <span class="price">Form $ 250</span>
                  </div>
                </div><!-- single-product end -->
                <div class="product-single-slide">
                  <a href="#0" class="item-link"></a>
                  <div class="thumb"><img src="assets/images/product/3.png" alt="image"></div>
                  <div class="content">
                    <h3 class="product-name">Mobile Case</h3>
                    <span class="price">Form $ 250</span>
                  </div>
                </div><!-- single-product end -->
                <div class="product-single-slide">
                  <a href="#0" class="item-link"></a>
                  <div class="thumb"><img src="assets/images/product/3.png" alt="image"></div>
                  <div class="content">
                    <h3 class="product-name">Mobile Case</h3>
                    <span class="price">Form $ 250</span>
                  </div>
                </div><!-- single-product end -->
                <div class="product-single-slide">
                  <a href="#0" class="item-link"></a>
                  <div class="thumb"><img src="assets/images/product/2.png" alt="image"></div>
                  <div class="content">
                    <h3 class="product-name">Custom Mug</h3>
                    <span class="price">Form $ 250</span>
                  </div>
                </div><!-- single-product end -->
              </div>
              <ul class="product-slider-nav">
                <li class="prev"><i class="fa fa-chevron-left"></i></li>
                <li class="next"><i class="fa fa-chevron-right"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- product-section end -->

    <!-- service-section start -->
    <section class="service-section clippy-shape pt-150 pb-150">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="section-header text-center text-white">
              <h2 class="section-title">Get your perfect solution from here</h2>
              <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin</p>
            </div>
          </div>
        </div>
        <div class="row mb-none-40">
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/1.png" alt="image"></div>
              <h4 class="service-name">Unique products</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/2.png" alt="image"></div>
              <h4 class="service-name">Mockup generator</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/3.png" alt="image"></div>
              <h4 class="service-name">Global prient network</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/4.png" alt="image"></div>
              <h4 class="service-name">24/7 Support</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/5.png" alt="image"></div>
              <h4 class="service-name">Manual orders</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
          <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="service-item text-white mb-40">
              <div class="icon"><img src="assets/images/icon/service/6.png" alt="image"></div>
              <h4 class="service-name">Free shipping</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adielit sed do eiusmod tempor incididunt ut labore et dolore magna aliquauis ipsum suspendisse</p>
            </div>
          </div><!-- service-item end -->
        </div>
      </div>
    </section>
    <!-- service-section end -->

    <!-- testimonial-section start -->
    <section class="testimonial-section pt-150">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-6">
            <div class="testimonial-area">
              <h2 class="section-title text-xl-left text-center mb-40">Happy client what`s say about Nepgeeks</h2>
              <div class="testimonial-slider">
                <div class="testimonial-single-slide">
                  <div class="item-header">
                    <div class="thumb"><img src="assets/images/testimonial/1.png" alt="image"></div>
                    <div class="content">
                      <h3 class="name">mabiya khan</h3>
                      <span class="designation">Business man</span>
                    </div>
                  </div>
                  <div class="item-conetnt">
                    <p>Posuere dapibus at nulla aliquam diam venicvlis at elit vitae mattis conubia. Cursus vivpulvilorem wisi dolor sed est luctus egestas a ptenmagnsem in lectus lobortis dictum suspendisse luctuvamus arcu sit viverra varius. Pharetra malesuadatrices at, mauris a quis vel cras .</p>
                    <div class="ratings">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <span class="rating-average">5.0</span>
                    </div>
                  </div>
                </div><!-- testimonial-single-slide end -->
                <div class="testimonial-single-slide">
                  <div class="item-header">
                    <div class="thumb"><img src="assets/images/testimonial/1.png" alt="image"></div>
                    <div class="content">
                      <h3 class="name">mabiya khan</h3>
                      <span class="designation">Business man</span>
                    </div>
                  </div>
                  <div class="item-conetnt">
                    <p>Posuere dapibus at nulla aliquam diam venicvlis at elit vitae mattis conubia. Cursus vivpulvilorem wisi dolor sed est luctus egestas a ptenmagnsem in lectus lobortis dictum suspendisse luctuvamus arcu sit viverra varius. Pharetra malesuadatrices at, mauris a quis vel cras .</p>
                    <div class="ratings">
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <i class="fa fa-star"></i>
                      <span class="rating-average">5.0</span>
                    </div>
                  </div>
                </div><!-- testimonial-single-slide end -->
              </div>
            </div>
          </div>
          <div class="col-xl-6">
            <div class="testimonial-thumb"><img src="assets/images/testimonial/thumb-1.png" alt="image"></div>
          </div>
        </div>
      </div>
    </section>
    <!-- testimonial-section end -->

    <!-- best-product-section start -->
    <section class="best-product-section pt-120">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="section-header text-center">
              <h2 class="section-title">Choose your best product</h2>
              <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/1.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Man t-Shirt</h3>
                <span class="price">Form $25.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/2.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Coffe cup</h3>
                <span class="price">Form $15.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/3.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Supper hat</h3>
                <span class="price">Form $10.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/4.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Women t-shirt</h3>
                <span class="price">Form $35.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/5.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Shopping bag</h3>
                <span class="price">Form $05.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/6.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Softking hoodie</h3>
                <span class="price">Form $75.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/7.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">Ladies bag</h3>
                <span class="price">Form $75.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="product-single text-center">
              <a href="#0" class="item-link"></a>
              <div class="thumb"><img src="assets/images/product/8.jpg" alt="image"></div>
              <div class="details">
                <h3 class="product-name">School bag</h3>
                <span class="price">Form $80.00</span>
              </div>
            </div>
          </div><!-- product-single end -->
        </div>
      </div>
    </section>
    <!-- best-product-section end -->

    <!-- about-section start -->
    <section class="about-section pt-150">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-5">
            <div class="row mb-none-30 align-items-center about-thumb-area">
              <div class="col-sm-6">
                <div class="about-thumb mb-30"><img src="assets/images/about/1.jpg" alt="image"></div>
              </div>
              <div class="col-sm-6">
                <div class="about-thumb mb-30"><img src="assets/images/about/2.jpg" alt="image"></div>
                <div class="about-thumb mb-30"><img src="assets/images/about/3.jpg" alt="image"></div>
              </div>
            </div>
          </div>
          <div class="col-xl-7">
            <div class="section-header section-header--two text-xl-left text-center">
              <h2 class="section-title">25+ years experience about print on demand</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscinelsed do eimod tempor incididunt ut labore .</p>
            </div>
            <div class="row mb-none-30">
              <div class="col-sm-6">
                <div class="about-single mb-30">
                  <h4 class="title">1995 Get iso Certifite</h4>
                  <p>Turpis ante turpis venenatisehi cula aliquam nam in molestie, turpis lobortis</p>
                </div>
              </div><!-- about-single end -->
              <div class="col-sm-6">
                <div class="about-single mb-30">
                  <h4 class="title"> Achived 2000m</h4>
                  <p>Turpis ante turpis venenatisehi cula aliquam nam in molestie, turpis lobortis</p>
                </div>
              </div><!-- about-single end -->
              <div class="col-sm-6">
                <div class="about-single mb-30">
                  <h4 class="title">2005 fixed with Shofy</h4>
                  <p>Turpis ante turpis venenatisehi cula aliquam nam in molestie, turpis lobortis</p>
                </div>
              </div><!-- about-single end -->
              <div class="col-sm-6">
                <div class="about-single mb-30">
                  <h4 class="title">Prienting show</h4>
                  <p>Turpis ante turpis venenatisehi cula aliquam nam in molestie, turpis lobortis</p>
                </div>
              </div><!-- about-single end -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- about-section end -->

    <!-- blog-section start -->
    <section class="blog-section pt-150 pb-150">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="section-header text-center">
              <h2 class="section-title">Our news update & tips</h2>
              <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin </p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 mb-lg-0 mb-5">
            <div class="post-single">
              <div class="post-thumb"><img src="assets/images/blog/b1.jpg" alt="image"></div>
              <div class="post-content">
                <h3 class="post-title"><a href="#0">Metus lorem libero venenatis nulla wislesquen wisi in libero gravida neque ante</a></h3>
                <div class="post-meta">
                  <a href="#0" class="post-date">25 jan 2020</a>
                  <ul class="post-meta-list">
                    <li><a href="#0">post by admin</a></li>
                    <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                    <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <ul class="small-post-list">
              <li>
                <div class="post-thumb"><img src="assets/images/blog/s1.jpg" alt="image"></div>
                <div class="post-content">
                  <h6 class="post-title"><a href="#0">pellentesqutempus Sapien egestas</a></h6>
                  <ul class="post-meta">
                    <li><a href="#0">post by admin</a></li>
                    <li><a href="#0">25 dec 2020</a></li>
                  </ul>
                </div>
              </li>
              <li>
                <div class="post-thumb"><img src="assets/images/blog/s2.jpg" alt="image"></div>
                <div class="post-content">
                  <h6 class="post-title"><a href="#0">aenean masselntumngn sed</a></h6>
                  <ul class="post-meta">
                    <li><a href="#0">post by admin</a></li>
                    <li><a href="#0">25 dec 2020</a></li>
                  </ul>
                </div>
              </li>
              <li>
                <div class="post-thumb"><img src="assets/images/blog/s3.jpg" alt="image"></div>
                <div class="post-content">
                  <h6 class="post-title"><a href="#0">fermentum anonum cquathes world class</a></h6>
                  <ul class="post-meta">
                    <li><a href="#0">post by admin</a></li>
                    <li><a href="#0">25 dec 2020</a></li>
                  </ul>
                </div>
              </li>
              <li>
                <div class="post-thumb"><img src="assets/images/blog/s4.jpg" alt="image"></div>
                <div class="post-content">
                  <h6 class="post-title"><a href="#0">sollicitudin tesque tesquenh our update</a></h6>
                  <ul class="post-meta">
                    <li><a href="#0">post by admin</a></li>
                    <li><a href="#0">25 dec 2020</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- blog-section end -->

    <!-- subscribe-section start -->
    <div class="subscribe-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h2 class="subscribe-title">Subscribe Newslatter?</h2>
            <form class="subscribe-form">
              <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
              <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- subscribe-section end -->
@endsection