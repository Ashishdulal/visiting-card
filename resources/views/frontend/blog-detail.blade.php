@extends('layouts.frontend.app')

@section('content')

 <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Blog Details</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li><a href="shop.html">Shop</a></li>
              <li>Shop details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- blog-details-section start -->
  <section class="blog-details-section pt-150 pb-150">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="blog-details-wrapper">
            <div class="blog-details-thumb"><img src="assets/images/blog/b2.jpg" alt="image"></div>
            <div class="blog-details-content">
              <h3 class="blog-details-title">Metus lorem libero venena islesqi libero gravida</h3>
              <p>Gravida fringilla in aliquet justo mus faucibus, dapibus dictum fusce integesittis libero posuere, montes ut dolor feugiat justo, taciti pede, aenean cras. Aliquam aliquet vitae tristique nibh, orci interdum. Donec in ornare velit. Integer quiequat orci ultricies at risus morbieu eros nulla enim per sed euismod nec pede, massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing Placerat nulla poritor dolor at, tellus elit lacus eget et, optio ut sollicitudin erat. Sed lacinia in iscesque ipsum maxime, sem natoque eget pede lectus facilisi, a mauris montes, nec erat erat lacinia, et tortor dignissim ut ut nulla dolor. Praesent scelerisque ligullaoreet tempus pede praesent, elit dui condimentum suscipit cras. Lectus eu penatibus malesuada in sollicitudin, viverra bibendum, at hendrerit eleifend lobortis partur leo, consequat consequat omnis nullam enim felis. Rhoncus pede euismod etium, cras fusce volutpat sed lorem ultricies, cupidatat amet. Faucibus a, ieptos lacinia in ut wisi est nec. Phasellus posuere tempor fringilla malesuada</p>
              <img src="assets/images/blog/b4.jpg" alt="image">
              <p>Metus dui orci mauris donec. Tincidunt mollis tortor ac pharetra quam taciti, nisl nulla, interdum commodo, congue ante laoreet inceptos et a, nec in urna. dolor fringilla, aliquam nulla mus, orci donec libero magna in, posuere nunc in maenas enim interdum lectus. Morbi varius, pulvinar et consequat sagittis sed euismod ut, duis in nisl massa. Feugiat vel luctus dui a lacinia quam, diam eget nonummy odio purus aenean. Urna eget vel non elit augue.</p>
              <ul>
                <li>Tincidunt mollis tortor ac pharetra quam taciti</li>
                <li>Consequat nonummy amet elementum</li>
                <li>fermentum nulla dictum nulla, aliquam sed</li>
                <li>pellentesque natoque, elementum ante</li>
              </ul>
              <p>Leo massa mauris. Nulla sint etiam cras sodales labore, ut in nec nulladtsaquet morbi posuere lorem, risus tincidunt scelerisque condimentum malesuadmetus. Habitasse urna ipsa suspendisse, mollis in curabitur metus. In arcu ornaraugue penatibus. massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing, blandit aenean vivamus amet lectus wisi aliquam. Adipiscing nam moletesque libero neque</p>
              <blockquote>
                <p>Gravida fringilla in aliquet justo mus fciddteger sagittis libero posuere, montes ut dolor fejusaciti pede aenean cras. Aliquam aliquet vitae tristique nibh orci interdum. onec in ornare velit. enim per sed euismod nec pede</p>
              </blockquote>
              <p>Eveniet etiam quis a, tempor eleifend arcu, pulvinar tempor suscipit mrisOrnare id lacus, ac ullamcorper non fringilla. Orci nonummy at, mattis vitae quisquubus quis magna, risus mi tortor arcu quam turpis, aliquam aliquam wisi eros ilectus. Sit massa curabitur ornare ipsum porttitor, fusce est, maecenas dolor eraliquet aliquam neque molestie.</p>
              <div class="post-meta border-top">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- blog-details-wrapper end -->
          <ul class="suggested-post mt-50">
            <li class="prev-post">
              <a href="#0"><span>previous</span>Interdum felis egetvelit print </a>
            </li>
            <li class="next-post">
              <a href="#0"><span>next</span>Ligula tempc acturpis eucursus duis</a>
            </li>
          </ul><!-- suggested-post end-->
          <div class="comments-area">
            <h3 class="title">2 comments</h3>
            <ul class="comments-list">
              <li class="single-comment">
                <div class="thumb">
                  <img src="assets/images/blog/comment1.png" alt="image">
                </div>
                <div class="content">
                  <h6 class="name">Will Marvin</h6>
                  <span class="date">1 day ago</span>
                  <a href="#0" class="reply-btn">Reply</a>
                  <p>magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                </div>
              </li><!-- single-review end -->
              <li class="single-comment">
                <div class="thumb">
                  <img src="assets/images/blog/comment2.png" alt="image">
                </div>
                <div class="content">
                  <h6 class="name">Mae Hayes</h6>
                  <span class="date">2 day ago</span>
                  <a href="#0" class="reply-btn">Reply</a>
                  <p>magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                </div>
              </li><!-- single-review end -->
            </ul>
          </div><!-- comments-area end -->
          <div class="comment-form-area">
            <h3 class="title">Live a Comment</h3>
            <form class="comment-form">
              <div class="row">
                <div class="col-lg-12 form-group">
                  <input type="text" name="comment-name" id="comment-name" placeholder="Your Name">
                </div>
                <div class="col-lg-12 form-group">
                  <input type="email" name="comment-email" id="comment-email" placeholder="Email Address">
                </div>
                <div class="col-lg-12 form-group">
                  <input type="url" name="comment-url" id="comment-url" placeholder="Web Site">
                </div>
                <div class="col-lg-12 form-group">
                  <textarea name="comment-message" id="comment-message" placeholder="Write Comment"></textarea>
                </div>
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-primary">post Comment</button>
                </div>
              </div>
            </form>
          </div><!-- comment-form-area end -->
        </div>
        <div class="col-lg-4">
          <div class="sidebar">
            <div class="widget search-widget">
              <form class="widget-search-form">
                <input type="text" name="widget-search_field" id="widget-search_field" placeholder="Search in here">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-file-text-o"></i> news categories</h3>
              <ul class="category-list">
                <li>
                  <a href="#0">T-shirt <span>30</span></a>
                </li>
                <li>
                  <a href="#0">mug printing <span>18</span></a>
                </li>
                <li>
                  <a href="#0">Shopping bag <span>32</span></a>
                </li>
                <li>
                  <a href="#0">payment <span>26</span></a>
                </li>
                <li>
                  <a href="#0">support <span>10</span></a>
                </li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-file-o"></i>recent post</h3>
              <ul class="small-post-list">
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s5.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s6.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s7.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s8.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-shield"></i> achived</h3>
              <ul class="achived-list">
                <li><a href="#0">january <span>2018</span></a></li>
                <li><a href="#0">march <span>2018</span></a></li>
                <li><a href="#0">jun <span>2018</span></a></li>
                <li><a href="#0">frebuary <span>2019</span></a></li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-tags"></i> news tag</h3>
              <div class="post-tags">
                <a href="#0">printing</a>
                <a href="#0">payment</a>
                <a href="#0">T- shirt</a>
                <a href="#0">research</a>
                <a href="#0">Custom Design</a>
              </div>
            </div><!-- widget end -->
          </div><!-- sidebar end -->
        </div>
      </div>
    </div>
  </section>
  <!-- blog-details-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection