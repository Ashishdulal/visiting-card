@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Shop details</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li><a href="shop.html">Shop</a></li>
              <li>Shop details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- custom-design-section start -->
  <section class="custom-design-section pt-150 pb-150">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <div class="sidebar-shop">
            <div class="widget">
              <h6 class="widget-title">Cetagories</h6>
              <form class="filter-form">
                <div class="radio-wrapper">
                  <label for="shirt">
                    <input type="radio" id="shirt" name="radio-group" checked>
                    <span class="fake-box"></span>
                    <span class="radio-text">T-shirt</span>
                  </label> 
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="bag">
                    <input type="radio" id="bag" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Shopping bag</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="mug">
                    <input type="radio" id="mug" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Mug</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="stationary">
                    <input type="radio" id="stationary" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Stationery</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="cap">
                    <input type="radio" id="cap" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Cap</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="tupi">
                    <input type="radio" id="tupi" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Tupi</span>
                  </label>
                </div><!-- radio-wrapper end -->
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h6 class="widget-title">Type</h6>
              <form class="filter-form">
                <div class="radio-wrapper">
                  <label for="men">
                    <input type="radio" id="men" name="radio-group" checked>
                    <span class="fake-box"></span>
                    <span class="radio-text">Men</span>
                  </label> 
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="women">
                    <input type="radio" id="women" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Women</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="child">
                    <input type="radio" id="child" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Child</span>
                  </label>
                </div><!-- radio-wrapper end -->
                <div class="radio-wrapper">
                  <label for="winter">
                    <input type="radio" id="winter" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">Winter</span>
                  </label>
                </div><!-- radio-wrapper end -->
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h6 class="widget-title">Color</h6>
              <div class="product-color-list">
                <a href="#0" class="color-1"></a>
                <a href="#0" class="color-2"></a>
                <a href="#0" class="color-3"></a>
                <a href="#0" class="color-4"></a>
                <a href="#0" class="color-5"></a>
                <a href="#0" class="color-6"></a>
                <a href="#0" class="color-7"></a>
                <a href="#0" class="color-8"></a>
              </div>
            </div><!-- widget end -->
          </div>
        </div><!-- sidebar-shop end -->
        <div class="col-lg-9">
          <div class="custom-design-wrapper">
            <div class="main-design-thumb">
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m1.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m2.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload2" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload2"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m3.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload3" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload3"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m4.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload4" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload4"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m1.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload5" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload5"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m2.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload6" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload6"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb">
                  <img src="assets/images/product/m3.png" alt="image">
                  <div class="avatar-upload">
                    <div class="avatar-edit">
                      <input type='file' class="imageUpload" id="imageUpload7" accept=".png, .jpg, .jpeg" />
                      <label for="imageUpload7"></label>
                    </div>
                    <div class="avatar-preview">
                      <div class="imagePreview"></div>
                    </div>
                  </div><!-- avatar-upload end -->
                </div>
              </div><!-- single-slide end -->
            </div><!-- main-design-thumb end -->

            <div class="main-design-thumb-nav">
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m1.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m2.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m3.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m4.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m1.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m2.png" alt="image"></div>
              </div><!-- single-slide end -->
              <div class="single-slide">
                <div class="thumb"><img src="assets/images/product/m3.png" alt="image"></div>
              </div><!-- single-slide end -->
            </div><!-- main-design-thumb-nav end -->
            <div class="btn-area mt-5">
              <a href="#0" class="btn btn-primary">add to basket</a>
              <a href="#0" class="btn btn-secondary">order now</a>
            </div>
          </div>

          <div class="product-review-area mt-100">
            <ul class="nav nav-tabs" id="productRevTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Review (03)</a>
              </li>
            </ul>
            <div class="tab-content" id="productRevTabContent">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="product-details-wrapper">
                  <p>Pellentesque at penatibus id, felis sociis dui pulvinar lacus accumsan, varius mauris, dolor convallis.oat sed nulla integer, nibh molestie orci pellentesque adipiscing mattis vitae, fermentum egestas cras, imtum ut metus amet, sed eu ut urna donec maecenas maecenas. Proin dolor donec ut sit, metus sit nullmdtiam eget, ullamcorper placerat. A ac platea aenean magna massa commodo, tincidunt mattis condimentum. Integer sem elementum, libero arcu vel quam, non ut sem iaculis ligula faucibus aenean. Molestie eros nunc  enim eu facilisis, in sodales arcu, libero ut molestie torquent nunc ipsum nulla, vestibulum magna doloribus proin scelerisque, ipsum eget ultricies egestas. aliquam sed turpis, nec nunc sit mauris non, ante vel auctor sed sagittis scelerisque parturient, mi ut ultricies.</p>
                  <ul class="product-details-list mt-20">
                    <li>Wholesale cost:from $10.30</li>
                    <li>Shipping:from $3.50</li>
                    <li>Manufacturing time:3 days</li>
                    <li>Available sizes:S, M, L, XL, 2XL, 3XL</li>
                    <li>Available brands:Bella+Canvas</li>
                    <li>Manufacturing countries:US, UK</li>
                  </ul>
                </div>
              </div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <ul class="review-list">
                  <li class="single-review">
                    <div class="thumb"><img src="assets/images/product/reviewer-1.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Will Marvin</h6>
                      <span class="review-time">1 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>(05)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                  <li class="single-review">
                    <div class="thumb"><img src="assets/images/product/reviewer-2.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Mae Hayes</h6>
                      <span class="review-time">2 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>(05)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                  <li class="single-review">
                    <div class="thumb"><img src="assets/images/product/reviewer-3.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Martin Hook</h6>
                      <span class="review-time">1 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                </ul>
              </div>
            </div>
          </div><!-- product-review-area end -->

          <div class="video-thumb main-overlay mt-100">
            <img src="assets/images/bg/video.jpg" alt="image">
            <a href="#0" class="video-icon"><i class="fa fa-play"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- custom-design-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection