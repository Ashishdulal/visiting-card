@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Blog post</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li><a href="shop.html">Shop</a></li>
              <li>Shop details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- blog-section start -->
  <section class="blog-section pt-150 pb-150">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="post-single mb-30">
            <div class="post-thumb"><img src="assets/images/blog/b2.jpg" alt="image"></div>
            <div class="post-content">
              <h3 class="post-title"><a href="#0">Metus lorem libero venena islesqi libero gravida</a></h3>
              <p>Gravida fringilla in aliquet justo mus faucibus, dapibus dictum fusce integesittis libero posuere, montes ut dolor feugiat justo, taciti pede, aenean cras. Aliquam aliquet vitae tristique nibh, orci interdum. Donec in ornare velit. Integer quiequat orci ultricies at risus morbieu eros nulla enim per sed euismod nec pede, massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing</p>
              <div class="post-meta border-top">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- post-single end-->
          <div class="post-single mb-30">
            <div class="post-thumb"><img src="assets/images/blog/b3.jpg" alt="image"></div>
            <div class="post-content">
              <h3 class="post-title"><a href="#0">Metus lorem libero venena islesqi libero gravida</a></h3>
              <p>Gravida fringilla in aliquet justo mus faucibus, dapibus dictum fusce integesittis libero posuere, montes ut dolor feugiat justo, taciti pede, aenean cras. Aliquam aliquet vitae tristique nibh, orci interdum. Donec in ornare velit. Integer quiequat orci ultricies at risus morbieu eros nulla enim per sed euismod nec pede, massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing</p>
              <div class="post-meta border-top">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- post-single end-->
          <div class="post-single mb-30">
            <div class="post-thumb"><img src="assets/images/blog/b4.jpg" alt="image"></div>
            <div class="post-content">
              <h3 class="post-title"><a href="#0">Metus lorem libero venena islesqi libero gravida</a></h3>
              <p>Gravida fringilla in aliquet justo mus faucibus, dapibus dictum fusce integesittis libero posuere, montes ut dolor feugiat justo, taciti pede, aenean cras. Aliquam aliquet vitae tristique nibh, orci interdum. Donec in ornare velit. Integer quiequat orci ultricies at risus morbieu eros nulla enim per sed euismod nec pede, massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing</p>
              <div class="post-meta border-top">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- post-single end-->
          <div class="post-single mb-30">
            <div class="post-thumb"><img src="assets/images/blog/b5.jpg" alt="image"></div>
            <div class="post-content">
              <h3 class="post-title"><a href="#0">Metus lorem libero venena islesqi libero gravida</a></h3>
              <p>Gravida fringilla in aliquet justo mus faucibus, dapibus dictum fusce integesittis libero posuere, montes ut dolor feugiat justo, taciti pede, aenean cras. Aliquam aliquet vitae tristique nibh, orci interdum. Donec in ornare velit. Integer quiequat orci ultricies at risus morbieu eros nulla enim per sed euismod nec pede, massa in mattis tincidunt lectus elit. Eros at nunc nam adipiscing</p>
              <div class="post-meta border-top">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div><!-- post-single end-->
          <nav class="d-pagination" aria-label="Page navigation example">
            <ul class="pagination justify-content-center flex-wrap">
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
            </ul>
          </nav>
        </div>
        <div class="col-lg-4">
          <div class="sidebar">
            <div class="widget search-widget">
              <form class="widget-search-form">
                <input type="text" name="widget-search_field" id="widget-search_field" placeholder="Search in here">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-file-text-o"></i> news categories</h3>
              <ul class="category-list">
                <li>
                  <a href="#0">T-shirt <span>30</span></a>
                </li>
                <li>
                  <a href="#0">mug printing <span>18</span></a>
                </li>
                <li>
                  <a href="#0">Shopping bag <span>32</span></a>
                </li>
                <li>
                  <a href="#0">payment <span>26</span></a>
                </li>
                <li>
                  <a href="#0">support <span>10</span></a>
                </li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-file-o"></i>recent post</h3>
              <ul class="small-post-list">
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s5.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s6.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s7.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="post-thumb"><img src="assets/images/blog/s8.jpg" alt="image"></div>
                  <div class="post-content">
                    <h6 class="post-title"><a href="#0">France Preparetoding Stake Its Place</a></h6>
                    <ul class="post-meta">
                      <li><a href="#0">post by admin</a></li>
                      <li><a href="#0">25 dec 2020</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-shield"></i> achived</h3>
              <ul class="achived-list">
                <li><a href="#0">january <span>2018</span></a></li>
                <li><a href="#0">march <span>2018</span></a></li>
                <li><a href="#0">jun <span>2018</span></a></li>
                <li><a href="#0">frebuary <span>2019</span></a></li>
              </ul>
            </div><!-- widget end -->
            <div class="widget">
              <h3 class="widget-title"><i class="fa fa-tags"></i> news tag</h3>
              <div class="post-tags">
                <a href="#0">printing</a>
                <a href="#0">payment</a>
                <a href="#0">T- shirt</a>
                <a href="#0">research</a>
                <a href="#0">Custom Design</a>
              </div>
            </div><!-- widget end -->
          </div><!-- sidebar end -->
        </div>
      </div>
    </div>
  </section>
  <!-- blog-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection