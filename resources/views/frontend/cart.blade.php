@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Cart Page</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li><a href="shop.html">Shop</a></li>
              <li>Shop details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- cart-section start -->
  <section class="cart-section pt-120 pb-120">
    <div class="container">
      <form class="cart-form">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="form-title">Billing Details</h3>
            <div class="form-row">
              <div class="col-lg-12 form-group">
                <input type="text" name="cart_fname" id="cart_fname" placeholder="First Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="cart_lname" id="cart_lname" placeholder="Last Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="cart_cname" id="cart_cname" placeholder="Company Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="email" name="cart_email" id="cart_email" placeholder="Email Address">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="cart_address" id="cart_address" placeholder="Address">
              </div>
              <div class="col-lg-6 form-group">
                <select name="cart_city" id="cart_city">
                  <option value="city">City</option>
                  <option value="noakhali">Noakhali</option>
                  <option value="chittagong">chittagong</option>
                  <option value="dhaka">dhaka</option>
                </select>
              </div>
              <div class="col-lg-6 form-group">
                <select name="cart_country" id="cart_country">
                  <option value="city">Select Country</option>
                  <option value="bangladesh">Bangladesh</option>
                  <option value="chittagong">chittagong</option>
                  <option value="dhaka">dhaka</option>
                </select>
              </div>
              <div class="col-lg-6 form-group">
                <input type="text" name="cart_zip" id="cart_zip" placeholder="Post Code / Zip">
              </div>
              <div class="col-lg-6 form-group">
                <input type="text" name="cart_phone" id="cart_phone" placeholder="Phone">
              </div>
            </div>
          </div>
          <div class="col-lg-6 mt-lg-0 mt-3">
            <h3 class="form-title">Shipping Details</h3>
            <div class="form-row">
              <div class="col-lg-12 form-group">
                <input type="text" name="shipping_fname" id="shipping_fname" placeholder="First Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="shipping_lname" id="shipping_lname" placeholder="Last Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="shipping_cname" id="shipping_cname" placeholder="Company Name">
              </div>
              <div class="col-lg-12 form-group">
                <input type="email" name="shipping_email" id="shipping_email" placeholder="Email Address">
              </div>
              <div class="col-lg-12 form-group">
                <input type="text" name="shipping_address" id="shipping_address" placeholder="Address">
              </div>
              <div class="col-lg-6 form-group">
                <select name="signup_city" id="signup_city">
                  <option value="city">City</option>
                  <option value="noakhali">Noakhali</option>
                  <option value="chittagong">chittagong</option>
                  <option value="dhaka">dhaka</option>
                </select>
              </div>
              <div class="col-lg-6 form-group">
                <select name="signup_country" id="signup_country">
                  <option value="city">Select Country</option>
                  <option value="bangladesh">Bangladesh</option>
                  <option value="chittagong">chittagong</option>
                  <option value="dhaka">dhaka</option>
                </select>
              </div>
              <div class="col-lg-6 form-group">
                <input type="text" name="signup_zip" id="signup_zip" placeholder="Post Code / Zip">
              </div>
              <div class="col-lg-6 form-group">
                <input type="text" name="signup_phone" id="signup_phone" placeholder="Phone">
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-100">
          <div class="col-lg-12">
            <div class="product-table-area table-responsive">
              <table class="product-table">
                <thead>
                  <tr>
                    <th>product</th>
                    <th>quntity</th>
                    <th>price</th>
                    <th>total</th>
                    <th>remove</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t1.png" alt="image"></div>
                        <span class="product-name">T - Shirt</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="2">
                      </div>
                    </td>
                    <td><span class="price">$35</span></td>
                    <td><span class="total-price">$70</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t2.png" alt="image"></div>
                        <span class="product-name">Mug</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="4">
                      </div>
                    </td>
                    <td><span class="price">$40</span></td>
                    <td><span class="total-price">$160</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t3.png" alt="image"></div>
                        <span class="product-name">Hodie</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="2">
                      </div>
                    </td>
                    <td><span class="price">$65</span></td>
                    <td><span class="total-price">$130</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t4.png" alt="image"></div>
                        <span class="product-name">Hat</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="1">
                      </div>
                    </td>
                    <td><span class="price">$45</span></td>
                    <td><span class="total-price">$45</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row mt-70">
          <div class="col-lg-6">
            <div class="cart-total-area">
              <h3 class="title">Cart Total</h3>
              <table class="cart-total-table">
                <thead>
                  <tr>
                    <th>Cart Total</th>
                    <th>Shipping and Handling</th>
                    <th>Oder Tottal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>$3,456</td>
                    <td>Free Shipping</td>
                    <td>$3,456</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="btn-area mt-3 text-right">
              <button type="submit" class="btn btn-primary">Producet Checkout</button>
            </div>
            <div class="order-note mt-70">
              <h3 class="title mb-20">Order note</h3>
              <textarea name="ordernote" id="ordernote"></textarea>
            </div>
          </div>
          <div class="col-lg-6 mt-lg-0 mt-4">
            <div class="payment-info">
              <h3 class="title">Payment Information</h3>
              <ul class="details-list">
                <li>
                  <span class="caption">Direct Bank Transfar</span>
                  <p>Muis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequ</p>
                </li>
                <li>
                  <span class="caption">Payoneer</span>
                  <p>Muis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequ</p>
                </li>
                <li>
                  <span class="caption">Paypal</span>
                  <p>Muis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequ</p>
                </li>
              </ul>
              <div class="payment-methods mt-30"><img src="assets/images/elements/payment-method.png" alt="image"></div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- cart-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->
  @endsection