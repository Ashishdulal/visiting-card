@extends('layouts.frontend.app')

@section('content')

<!-- inner-hero-section start -->
<section class="inner-hero-section" style="background-image: url('/assets/images/bg/inner-hero.jpg');">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="inner-hero-content text-center">
          <h2 class="inner-hero-title">Shop details</h2>
          <ul class="page-links">
            <li><a href="/">Home</a></li>
            <li><a href="/product">Shop</a></li>
            <li>Shop details</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- inner-hero-section end -->

<!-- shop-details-section start -->
<section class="shop-details-section pt-150 pb-150">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 order-lg-1 order-2" id="productDetail">
        <div class="product-details-area">
          <div class="product-thumb-slider-area">
            <div class="product-details-thumb-slider">
              <div class="single-slide"><img src="/uploads/product/{{$product->product_image1}}" alt="image"></div>
              <div class="single-slide"><img src="/uploads/product/{{$product->product_image2}}" alt="image"></div>
              <div class="single-slide"><img src="/uploads/product/{{$product->product_image3}}" alt="image"></div>
            </div>
            <div class="product-details-slider-nav mt-50">
              <div class="single-nav"><img src="/uploads/product/{{$product->product_image1}}" alt="image"></div>
              <div class="single-nav"><img src="/uploads/product/{{$product->product_image2}}" alt="image"></div>
              <div class="single-nav"><img src="/uploads/product/{{$product->product_image3}}" alt="image"></div>
            </div>
          </div><!-- product-thumb-slider end -->
          <div class="product-details-content">
            <h3 class="product-name">{{$product->product_name}}</h3><br>
            <?php $excerpt = $product->product_description;
              $the_str = substr($excerpt, 0, 100);
              echo $the_str.'...';?><br><br>
              <div class="product-variation">
            <span class="product-price">Quantity:</span>
            <select id="quantitySelect">
              @foreach($productVar as $var)
              @if($var->category_id == $product->cat_id)
              <option value="{{$var->id}}">{{$var->product_quantity}}</option>
              @endif
              @endforeach
            </select><br>
          </div>
<!--               <div class="product-ratings mb-30">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </div> -->
              @foreach($firstVar as $var)
              @if($var->category_id == $product->cat_id)
              <?php $count=1;?>
              <div class="product-variation">
              <span>Price : Rs.</span>
              <input type="text" id="variationPrice" readonly name="productPrice" value="{{$var->product_price}}">
              </div>
              <div class="product-variation">
              <span>Product Thickness :</span>
              <input type="text" id="variationType" readonly value="{{$var->product_thickness}}">
              </div>
              <div class="product-variation">
              <span>Product Side :</span>
              <input type="text" id="variationSide" readonly value="{{$var->product_sides}}">
              </div>

              <?php if ($count>=1) {
                break;
              };?>
              @endif
              @endforeach
              <h6 class="widget-title">Product Type: {{$product->product_type}}</h6><br>
              <h6 class="widget-title">Product code: {{$product->product_code}}</h6>
              <form class="product-details-form">
                <div class="quantity rapper-quantity">
                  <input type="number" min="0" max="100" step="1" value="1">
                </div>
                <a href="#0" class="btn btn-primary">add to cart</a>
              </form>
            </div><!-- product-details-content end -->
          </div><!-- product-details-area end -->
          <script type="text/javascript">

              $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
              });

              $("#quantitySelect").on("change", function () {
                var varId = $(this).val();
               // alert(varId);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/variation-search/',
                  type: 'GET',
                  dataType: "json",
                  data: {
                    varId: varId
                  },
                  success: function (ret_data) {
                    console.log(ret_data);
                    document.getElementById('variationPrice').value = ret_data['product_price'];
                    document.getElementById('variationType').value = ret_data['product_thickness'];
                    document.getElementById('variationSide').value = ret_data['product_sides'];
                  }
                });
              });


            </script>
          <div class="product-review-area mt-100">
            <ul class="nav nav-tabs" id="productRevTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Review (03)</a>
              </li>
            </ul>
            <div class="tab-content" id="productRevTabContent">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="product-details-wrapper">
                  <?php echo ($product->product_description)?>
                </div>
              </div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <ul class="review-list">
                  <li class="single-review">
                    <div class="thumb"><img src="{{asset('assets/images/product/reviewer-1')}}.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Will Marvin</h6>
                      <span class="review-time">1 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>(05)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                  <li class="single-review">
                    <div class="thumb"><img src="{{asset('assets/images/product/reviewer-2')}}.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Mae Hayes</h6>
                      <span class="review-time">2 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>(05)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                  <li class="single-review">
                    <div class="thumb"><img src="{{asset('assets/images/product/reviewer-3')}}.png" alt="image"></div>
                    <div class="content">
                      <h6 class="name">Martin Hook</h6>
                      <span class="review-time">1 Day ago</span>
                      <p>Magnis nisl lectus velit parturient vitae, suspendisse fusce in variultusetvnea. Hac metus dui facilisis.</p>
                      <div class="ratings">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        <span>(4.5)</span>
                      </div>
                    </div>
                  </li><!-- single-review end -->
                </ul>
              </div>
            </div>
          </div><!-- product-review-area end -->
        </div>
        <div id="productSearchContent" class="col-lg-9" ></div>
        <div class="col-lg-3 order-lg-2 order-1">
          <div class="sidebar-shop">
            <div class="widget">
              <h6 class="widget-title">Categories</h6>
              <form class="filter-form">

                <div class="radio-wrapper">
                  <label for="shirt">
                    <input type="radio" id="shirt" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">
                      <a href="#" onclick="callAjax(event,'all', 0)">All Products</a>
                    </span>
                  </label> 
                </div><!-- radio-wrapper end -->
                 @foreach($productCat as $cat)
                <div class="radio-wrapper">
                  <label for="shirt">
                    <input type="radio" id="shirt" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text"><a href="#" onclick="callAjax(event,{{$cat->id}}, 0)">{{$cat->category_name}}</a></span>
                  </label> 
                </div><!-- radio-wrapper end -->
                @endforeach
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h6 class="widget-title">Type</h6>
              <form class="filter-form">
                 @foreach($productType as $type)
                <div class="radio-wrapper">
                  <label for="type">
                    <input type="radio" id="type" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text"><a href="#" onclick="callAjax(event, 0, {{$type->id}})">{{$type->product_type}}</a></span>
                  </label> 
                </div><!-- radio-wrapper end -->
                @endforeach
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <!-- <h6 class="widget-title">Color</h6>
              <div class="product-color-list">
                <a href="#0" class="color-1"></a>
                <a href="#0" class="color-2"></a>
                <a href="#0" class="color-3"></a>
                <a href="#0" class="color-4"></a>
                <a href="#0" class="color-5"></a>
                <a href="#0" class="color-6"></a>
                <a href="#0" class="color-7"></a>
                <a href="#0" class="color-8"></a>
              </div> -->
            </div><!-- widget end -->
          </div>
        </div><!-- sidebar-shop end -->
      </div>
                <div class="related-product-wrapper mt-100">
            <h3 class="title">More product</h3>
            <div class="related-product-slider-wrapper">
              <div class="related-product-slider">
                @foreach($otherProduct as $oProd)
                @if($product->id != $oProd->id)
                <div class="single-slide">
                  <div class="product-single text-center">
                    <a href="/product-details/{{$oProd->id}}" class="item-link"></a>
                    <div class="thumb"><img src="/uploads/product/{{$oProd->product_image3}}" alt="image"></div>
                    <div class="details">
                      <h3 class="product-name">{{$oProd->product_name}}</h3>
                      <span class="price">{{$oProd->product_code}}</span>
                    </div>
                  </div>
                </div><!-- single-slide -->
                @endif
                @endforeach
              </div>
            </div><!-- related-product-slider-wrapper end -->
          </div>
    </div>
  </section>
  <!-- shop-details-section end -->
  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="/assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->
 <script type="text/javascript">
    function callAjax(evt, id, type_id){
      evt.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: 'GET',
        beforeSend: function(){
         $('#preLoader').css("display", "block");
         $('#preLoader').css("opacity", "1");
       },
       url: '/search-category/',
       data: {'id': id ,'type_id': type_id},
       success: function (response) {
        $('#productSearchContent').show();
        $('#productSearchContent').html(response);
        $('#productDetail').hide();
        
      },
      complete: function(){
        $('#preLoader').css("display", "none");
        $('#preLoader').css("opacity", "0");
      }
    });
      
    }
  </script>
  @endsection