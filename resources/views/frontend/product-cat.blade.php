
@if($showName == 0)
<h4 class="text-header">All Products</h4>
@endif
@foreach($productCat as $cat)
<h4 class="text-cat-name">{{$cat->category_name}}:</h4><br>
<div class="row">
  @if(count($products))
  @foreach($products as $prod)
  <div class="col-lg-4 col-md-4 col-sm-6">
    <div class="product-single text-center">
      <a href="/product-details/{{$prod->id}}" class="item-link"></a>
      <div class="thumb"><img src="/uploads/product/{{$prod->product_image3}}" alt="image"></div>
      <div class="details">
        <h3 class="product-name">{{$prod->product_name}}</h3>
        <span class="price">Code: {{$prod->product_code}}</span>
      </div>
    </div>
  </div>
  @endforeach<!-- product-single end -->
  @else
  <h6 class="no-text">No Products Found On The List.</h6>
  @endif
</div>
@endforeach
