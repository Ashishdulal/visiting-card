@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">About company</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li>about</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- section start  -->
  <section class="pt-150 pb-150">
    <div class="container">
      <div class="row justify-content-lg-between align-items-center">
        <div class="col-xl-5">
          <div class="custom-design-thumb">
            <div class="shape"><img src="assets/images/elements/bg-shape-1.png" alt="image"></div>
            <div class="man"><img src="assets/images/elements/custom-design-man.png" alt="image"></div>
          </div>
        </div>
        <div class="col-xl-7">
          <div class="custom-design-content">
            <div class="section-header">
              <h2 class="section-title text-transform-normal"><span>250+ Products</span> in here for your custom design</h2>
              <p>Egestas. Mauris nec dolor vestibulum metus egstetrum erat elementum sed dui ipsuenas.</p>
            </div>
            <p>Dapibus ipsum dui nec in. Turpis et suspendisse ac pretiuconguptium, sed a aliquam blandit, erat nunc nascetur, donec quis placerat ante. Ac velit letesque posuere eget praesent sagittis turpis, torquent sem maecenas id. Libero lectus vitae, wisi ullamcorper leo porttitor, nibh ultricies malesuada, ullamcorper et cinia commodo</p>
            <div class="btn-area">
              <a href="#0" class="btn btn-secondary">shop now design</a>
              <a href="#0" class="btn btn-primary">create own design</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section end -->

  <!-- choose-section start -->
  <section class="choose-section pt-150 pb-150" style="background-image: url('assets/images/bg/choose-us-bg.png');">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-4">
          <div class="choose-us-thumb">
            <div class="main"><img src="assets/images/elements/choose-us-woman.png" alt="image"></div>
            <div class="shape">
              <img src="assets/images/elements/choose-us-object.png" alt="image">
              <div class="content">
                <span class="discount-rate">25% off</span>
                <p>for all product</p>
              </div>
            </div>
            <span class="thumb-title">unique design</span>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="choose-us-content">
            <h2 class="section-title text-white mb-40 text-transform-normal">Why you choose Nepgeeks for print on demand</h2>
            <div class="row mb-none-50">
              <div class="col-lg-6 col-md-6">
                <div class="choose-us-item mb-50">
                  <div class="item-header">
                    <div class="icon"><i class="flaticon-business-and-finance"></i></div>
                    <h3 class="title">Free signup</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ristipblditurpis erat, vivamus taciti laoreet iacuuis donec cum mauris amet aliquet</p>
                </div>
              </div><!-- choose-us-item end -->
              <div class="col-lg-6 col-md-6">
                <div class="choose-us-item mb-50">
                  <div class="item-header">
                    <div class="icon"><i class="flaticon-box"></i></div>
                    <h3 class="title">Low shipping</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ristipblditurpis erat, vivamus taciti laoreet iacuuis donec cum mauris amet aliquet</p>
                </div>
              </div><!-- choose-us-item end -->
              <div class="col-lg-6 col-md-6">
                <div class="choose-us-item mb-50">
                  <div class="item-header">
                    <div class="icon"><i class="flaticon-order"></i></div>
                    <h3 class="title">Automated order</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ristipblditurpis erat, vivamus taciti laoreet iacuuis donec cum mauris amet aliquet</p>
                </div>
              </div><!-- choose-us-item end -->
              <div class="col-lg-6 col-md-6">
                <div class="choose-us-item mb-50">
                  <div class="item-header">
                    <div class="icon"><i class="flaticon-height"></i></div>
                    <h3 class="title">No minimums</h3>
                  </div>
                  <p>Curabitur laoreet adipiscing. ristipblditurpis erat, vivamus taciti laoreet iacuuis donec cum mauris amet aliquet</p>
                </div>
              </div><!-- choose-us-item end -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- choose-section end -->

  <!-- mission-vission-section start -->
  <section class="mission-vission-section pt-150">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="mission-vission-thumb">
            <div class="el-1"><img src="assets/images/elements/happy-shopping/el-1.png" alt="image"></div>
            <div class="el-2"><img src="assets/images/elements/happy-shopping/el-2.png" alt="image"></div>
            <div class="el-3"><img src="assets/images/elements/happy-shopping/el-3.png" alt="image"></div>
            <div class="el-4"><img src="assets/images/elements/happy-shopping/el-4.png" alt="image"></div>
            <div class="el-5"><img src="assets/images/elements/happy-shopping/el-5.png" alt="image"></div>
            <div class="el-6"><img src="assets/images/elements/happy-shopping/el-6.png" alt="image"></div>
            <div class="el-7"><img src="assets/images/elements/happy-shopping/el-7.png" alt="image"></div>
            <div class="el-8"><img src="assets/images/elements/happy-shopping/el-8.png" alt="image"></div>
            <div class="el-9"><img src="assets/images/elements/happy-shopping/el-9.png" alt="image"></div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="mission-vission-content">
            <h2 class="section-title text-transform-normal mb-30">Our company mission & vission</h2>
            <p>Printing and typesetting industry. Lorem Ipsum has been the industrystaardummy text ever since the 1500s, when an unknown printer took a galleotype and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.There are many variations of passages of Lorem Ipsum available, but tma</p>
            <div class="row mt-30 mb-none-30">
              <div class="col-sm-6">
                <div class="text-item mb-30">
                  <h3 class="title">Our mission</h3>
                  <p>Explorer of the truth, the matuilder of human hapness. one rejsdislikes or avoids pleasure itselfbecause.</p>
                </div>
              </div><!-- text-item end -->
              <div class="col-sm-6">
                <div class="text-item mb-30">
                  <h3 class="title">Our vission</h3>
                  <p>Explorer of the truth, the matuilder of human hapness. one rejsdislikes or avoids pleasure itselfbecause.</p>
                </div>
              </div><!-- text-item end -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- mission-vission-section end -->

  <!-- how-work-section start -->
  <section class="how-work-section pt-150">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="section-header text-center">
            <h2 class="section-title">How to work</h2>
            <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="video-thumb">
            <img src="assets/images/bg/video.jpg" alt="image">
            <a href="https://www.youtube.com/embed/GT6-H4BRyqQ" data-rel="lightcase:myCollection" class="video-icon"><i class="fa fa-play"></i></a>
          </div>
        </div>
      </div>
      <div class="row pt-150 mb-none-30 position-relative">
        <div class="item-line-1"><img src="assets/images/elements/line-1.png" alt="image"></div>
        <div class="item-line-2"><img src="assets/images/elements/line-2.png" alt="image"></div>
        <div class="col-lg-4 col-md-6 mb-30">
          <div class="how-work-item">
            <div class="icon-thumb">
              <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
              <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
              <div class="icon"><img src="assets/images/icon/how-work/1.png" alt="image"></div>
            </div>
            <div class="content mt-30">
              <h3 class="title mb-15">Choose product</h3>
              <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
            </div>
          </div>
        </div><!-- how-work-item end -->
        <div class="col-lg-4 col-md-6 mb-30">
          <div class="how-work-item">
            <div class="icon-thumb">
              <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
              <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
              <div class="icon"><img src="assets/images/icon/how-work/2.png" alt="image"></div>
            </div>
            <div class="content mt-30">
              <h3 class="title mb-15">Create design</h3>
              <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
            </div>
          </div>
        </div><!-- how-work-item end -->
        <div class="col-lg-4 col-md-6 mb-30">
          <div class="how-work-item">
            <div class="icon-thumb">
              <div class="shape"><img src="assets/images/elements/how-work-icon-shape.png" alt="image"></div>
              <div class="shape-hover"><img src="assets/images/elements/how-work-icon-shape-hover.png" alt="image"></div>
              <div class="icon"><img src="assets/images/icon/how-work/3.png" alt="image"></div>
            </div>
            <div class="content mt-30">
              <h3 class="title mb-15">Home delivery</h3>
              <p>Quis lectus sagittis mattis nundolor lacus, vel mi aliquam eu ad asattis nec dolor lorem ullamcorper</p>
            </div>
          </div>
        </div><!-- how-work-item end -->
      </div>
    </div>
  </section>
  <!-- how-work-section end -->

  <!-- blog-section start -->
  <section class="blog-section pt-150 pb-150">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <h2 class="section-title">Our news update & tips</h2>
            <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 mb-lg-0 mb-5">
          <div class="post-single">
            <div class="post-thumb"><img src="assets/images/blog/b1.jpg" alt="image"></div>
            <div class="post-content">
              <h3 class="post-title"><a href="#0">Metus lorem libero venenatis nulla wislesquen wisi in libero gravida neque ante</a></h3>
              <div class="post-meta">
                <a href="#0" class="post-date">25 jan 2020</a>
                <ul class="post-meta-list">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0"><i class="fa fa-comment"></i></a><span class="comment-amount">03</span></li>
                  <li><a href="#0"><i class="fa fa-share-alt"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <ul class="small-post-list">
            <li>
              <div class="post-thumb"><img src="assets/images/blog/s1.jpg" alt="image"></div>
              <div class="post-content">
                <h6 class="post-title"><a href="#0">pellentesqutempus Sapien egestas</a></h6>
                <ul class="post-meta">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0">25 dec 2020</a></li>
                </ul>
              </div>
            </li>
            <li>
              <div class="post-thumb"><img src="assets/images/blog/s2.jpg" alt="image"></div>
              <div class="post-content">
                <h6 class="post-title"><a href="#0">aenean masselntumngn sed</a></h6>
                <ul class="post-meta">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0">25 dec 2020</a></li>
                </ul>
              </div>
            </li>
            <li>
              <div class="post-thumb"><img src="assets/images/blog/s3.jpg" alt="image"></div>
              <div class="post-content">
                <h6 class="post-title"><a href="#0">fermentum anonum cquathes world class</a></h6>
                <ul class="post-meta">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0">25 dec 2020</a></li>
                </ul>
              </div>
            </li>
            <li>
              <div class="post-thumb"><img src="assets/images/blog/s4.jpg" alt="image"></div>
              <div class="post-content">
                <h6 class="post-title"><a href="#0">sollicitudin tesque tesquenh our update</a></h6>
                <ul class="post-meta">
                  <li><a href="#0">post by admin</a></li>
                  <li><a href="#0">25 dec 2020</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- blog-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->
@endsection