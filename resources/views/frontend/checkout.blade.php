@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Checkout</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li><a href="shop.html">Shop</a></li>
              <li>Shop details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- checkout-section start -->
  <section class="checkout-section pt-150 pb-150">
    <div class="container">
      <form class="checkout-form">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-table-area table-responsive">
              <table class="product-table">
                <thead>
                  <tr>
                    <th>product</th>
                    <th>quntity</th>
                    <th>price</th>
                    <th>total</th>
                    <th>remove</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t1.png" alt="image"></div>
                        <span class="product-name">T - Shirt</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="2">
                      </div>
                    </td>
                    <td><span class="price">$35</span></td>
                    <td><span class="total-price">$70</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t2.png" alt="image"></div>
                        <span class="product-name">Mug</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="4">
                      </div>
                    </td>
                    <td><span class="price">$40</span></td>
                    <td><span class="total-price">$160</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t3.png" alt="image"></div>
                        <span class="product-name">Hodie</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="2">
                      </div>
                    </td>
                    <td><span class="price">$65</span></td>
                    <td><span class="total-price">$130</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                  <tr>
                    <td>
                      <div class="product">
                        <div class="product-thumb"><img src="assets/images/product/t4.png" alt="image"></div>
                        <span class="product-name">Hat</span>
                      </div>
                    </td>
                    <td>
                      <div class="quantity rapper-quantity">
                        <input type="number" min="0" max="100" step="1" value="1">
                      </div>
                    </td>
                    <td><span class="price">$45</span></td>
                    <td><span class="total-price">$45</span></td>
                    <td><button type="button" class="delete-btn"><i class="fa fa-trash"></i></button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row mt-50">
          <div class="col-lg-6">
            <div class="coupon-wrapper">
            <input type="text" name="coupon_field" id="coupon_field" placeholder="Enter Coupon Number">
              <button type="button" class="btn btn-primary">apply coupon</button>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="product-update-wrapper text-lg-right btn-area mt-lg-0 mt-3">
              <button type="button" class="btn border-btn">Update Cart</button>
              <button type="submit" class="btn btn-primary">Producet Checkout</button>
            </div>
          </div>
        </div>
        <div class="row mt-100">
          <div class="col-lg-6">
            <div class="shipping-details">
              <h3 class="title">Calulate Shipping</h3>
              <div class="form-row">
                <div class="form-group col-lg-12">
                  <select name="shipping_country" id="shipping_country">
                    <option value="#0">Select Country</option>
                    <option value="">Bangladesh</option>
                    <option value="">Dubai</option>
                    <option value="">Sudia Arab</option>
                  </select>
                </div>
                <div class="form-group col-lg-12">
                  <select name="shipping_state" id="shipping_state">
                    <option value="#0">State</option>
                    <option value="">Noakhali</option>
                    <option value="">Dhaka</option>
                    <option value="">Gazipur</option>
                  </select>
                </div>
                <div class="form-group col-lg-12">
                  <input type="text" name="shipping_zip" id="shipping_zip" placeholder="Post Code / Zip">
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 mt-lg-0 mt-3">
            <div class="cart-total-area">
              <h3 class="title">Cart Total</h3>
              <table class="cart-total-table">
                <thead>
                  <tr>
                    <th>Cart Total</th>
                    <th>Shipping and Handling</th>
                    <th>Oder Tottal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>$3,456</td>
                    <td>Free Shipping</td>
                    <td>$3,456</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="btn-area mt-3 text-lg-right">
              <button type="submit" class="btn btn-primary">Producet Checkout</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- checkout-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection