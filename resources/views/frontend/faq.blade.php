@extends('layouts.frontend.app')

@section('content')

<!-- login-section start -->
<div class="login-section">
  <div class="section-bg-image" style="background-image: url('assets/images/bg/inner-hero.jpg');"></div>
  <div class="login-area">
    <div class="login-close"></div>
    <div class="login-wrapper">
      <div class="text-center">
        <h3 class="title text-center">Hi, welcome to Nepgeeks</h3>
        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibusnon</p>
      </div>
      <form class="signin-form">
        <div class="form-row justify-content-between">
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="login_email" id="login_email" placeholder="Email or User Name">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="login_pass" id="login_pass" placeholder="Password">
          </div>
          <div class="col-lg-12 text-center mt-4">
            <a href="#0" class="btn btn-primary">Signin Now</a>
          </div>
        </div>
      </form>
      <div class="form-footer text-center mt-20">
        <a href="#0" class="forget-pass">Forget my password</a>
        <span class="mt-40">Signin with</span>
        <ul class="d-flex justify-content-center">
          <li><a href="#0"><img src="assets/images/icon/signin-signup/google.png" alt="image"></a></li>
          <li><a href="#0"><img src="assets/images/icon/signin-signup/facebook.png" alt="image"></a></li>
          <li><a href="#0"><img src="assets/images/icon/signin-signup/twitter.png" alt="image"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- login-section end -->

<!-- signup-section start -->
<div class="signup-section">
  <div class="section-bg-image" style="background-image: url('assets/images/bg/inner-hero.jpg');"></div>
  <div class="signup-area">
    <div class="signup-close">
    </div>
    <div class="signup-wrapper">
      <div class="text-center">
        <h3 class="title text-center">Hi, welcome to Nepgeeks</h3>
        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibusnon</p>
      </div>
      <form class="signup-form">
        <div class="form-row justify-content-between">
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_email" id="signup_email" placeholder="Email or User Name">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_pass" id="signup_pass" placeholder="Password">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_re_pass" id="signup_re_pass" placeholder="Re-password">
          </div>
          <div class="col-lg-12 text-center mt-4">
            <a href="#0" class="btn btn-primary">Signup Now</a>
          </div>
        </div>
      </form>
      <div class="form-footer text-center mt-20">
        <p>Already i have an account in here <a href="#0">Signin</a></p>
        <span class="mt-40">Signin with</span>
        <ul class="d-flex justify-content-center">
          <li><a href="#0"><img src="assets/images/icon/signin-signup/google.png" alt="image"></a></li>
          <li><a href="#0"><img src="assets/images/icon/signin-signup/facebook.png" alt="image"></a></li>
          <li><a href="#0"><img src="assets/images/icon/signin-signup/twitter.png" alt="image"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- signup-section end -->

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Frequently asked questions</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li>faq</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- faq-section start -->
  <section class="faq-section pt-150 pb-150">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-header text-center">
            <h2 class="section-title">Have any question</h2>
            <p>Placerat metus rhoncus cras netus veniam, sed odio. Id non vestibuluvellus vehicula curabitur lorem pulvinar rutrum netus.sollicitudin </p>
          </div>
          <div class="faq-wrapper mt-50">
            <ul class="nav justify-content-center mb-50" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">general</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="shipping-tab" data-toggle="tab" href="#shipping" role="tab" aria-controls="shipping" aria-selected="false">shipping</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false">payment</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="privacy" aria-selected="false">privacy</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="career-tab" data-toggle="tab" href="#career" role="tab" aria-controls="career" aria-selected="false">career</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                <div class="accordion cmn-accordion" id="faqAccordion-two">
                  <div class="card">
                    <div class="card-header" id="h-one">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-one" aria-expanded="false" aria-controls="c-one">
                        <span class="text">What is print on demand?</span>
                      </button>
                    </div>
                    <div id="c-one" class="collapse" aria-labelledby="h-one" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-two">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-two" aria-expanded="false" aria-controls="c-two">
                        <span class="text">How to work?</span>
                      </button>
                    </div>
                    <div id="c-two" class="collapse" aria-labelledby="h-two" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-three">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-three" aria-expanded="false" aria-controls="c-three">
                        <span class="text">What is print on demand?</span>
                      </button>
                    </div>
                    <div id="c-three" class="collapse" aria-labelledby="h-three" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-four">
                      <button class="acc-btn" type="button" data-toggle="collapse" data-target="#c-four" aria-expanded="true" aria-controls="c-four">
                        <span class="text">How to work?</span>
                      </button>
                    </div>
                    <div id="c-four" class="collapse show" aria-labelledby="h-four" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-five">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-five" aria-expanded="false" aria-controls="c-five">
                        <span class="text">Whre we are provided our service?</span>
                      </button>
                    </div>
                    <div id="c-five" class="collapse" aria-labelledby="h-five" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-six">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-six" aria-expanded="false" aria-controls="c-six">
                        <span class="text">Why you choose Nepgeeks?</span>
                      </button>
                    </div>
                    <div id="c-six" class="collapse" aria-labelledby="h-six" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-seven">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-seven" aria-expanded="false" aria-controls="c-seven">
                        <span class="text">Our marketing process?</span>
                      </button>
                    </div>
                    <div id="c-seven" class="collapse" aria-labelledby="h-seven" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-eight">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-eight" aria-expanded="false" aria-controls="c-eight">
                        <span class="text">Check our privacy & policy before product order?</span>
                      </button>
                    </div>
                    <div id="c-eight" class="collapse" aria-labelledby="h-eight" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-nine">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-nine" aria-expanded="false" aria-controls="c-nine">
                        <span class="text">Why you choose Nepgeeks?</span>
                      </button>
                    </div>
                    <div id="c-nine" class="collapse" aria-labelledby="h-nine" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-ten">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-ten" aria-expanded="false" aria-controls="c-ten">
                        <span class="text">Our marketing process?</span>
                      </button>
                    </div>
                    <div id="c-ten" class="collapse" aria-labelledby="h-ten" data-parent="#faqAccordion-two">
                      <div class="card-body">
                        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibus, nomassa imperdiet nibh vel ac. Gravida sit faucibus quisque rhoncus mattis quis, magna mauris dui viverra diam diam ultrices, pulvinar nulla egestas luctus hendrerit ultricies, dui rhoncus. Consectetuer tempus orci quis sit tempus, phasellus nec vestibulum mauris. Rutrum mollit auctor lectus fringilla </p>
                      </div>
                    </div>
                  </div><!-- card end -->
                </div>
              </div>
              <div class="tab-pane fade" id="shipping" role="tabpanel" aria-labelledby="shipping-tab">
                <div class="accordion cmn-accordion" id="faqAccordion-three">
                  <div class="card">
                    <div class="card-header" id="h-1">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-1" aria-expanded="false" aria-controls="c-1">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-1" class="collapse" aria-labelledby="h-1" data-parent="#faqAccordion-three">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-2">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-2" aria-expanded="false" aria-controls="c-2">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-2" class="collapse" aria-labelledby="h-2" data-parent="#faqAccordion-three">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-3">
                      <button class="acc-btn" type="button" data-toggle="collapse" data-target="#c-3" aria-expanded="true" aria-controls="c-3">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-3" class="collapse show" aria-labelledby="h-3" data-parent="#faqAccordion-three">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-4">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-4" aria-expanded="false" aria-controls="c-4">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-4" class="collapse" aria-labelledby="h-4" data-parent="#faqAccordion-three">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-5">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-5" aria-expanded="false" aria-controls="c-5">
                        <span class="text">Whre we are provided our service?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-5" class="collapse" aria-labelledby="h-5" data-parent="#faqAccordion-three">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                </div>
              </div>
              <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                <div class="accordion cmn-accordion" id="faqAccordion-four">
                  <div class="card">
                    <div class="card-header" id="h-6">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-6" aria-expanded="false" aria-controls="c-6">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-6" class="collapse" aria-labelledby="h-6" data-parent="#faqAccordion-four">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-7">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-7" aria-expanded="false" aria-controls="c-7">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-7" class="collapse" aria-labelledby="h-7" data-parent="#faqAccordion-four">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-8">
                      <button class="acc-btn" type="button" data-toggle="collapse" data-target="#c-8" aria-expanded="true" aria-controls="c-8">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-8" class="collapse show" aria-labelledby="h-8" data-parent="#faqAccordion-four">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-9">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-9" aria-expanded="false" aria-controls="c-9">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-9" class="collapse" aria-labelledby="h-9" data-parent="#faqAccordion-four">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-10">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-10" aria-expanded="false" aria-controls="c-10">
                        <span class="text">Whre we are provided our service?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-10" class="collapse" aria-labelledby="h-10" data-parent="#faqAccordion-four">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                </div>
              </div>
              <div class="tab-pane fade" id="privacy" role="tabpanel" aria-labelledby="privacy-tab">
                <div class="accordion cmn-accordion" id="faqAccordion-five">
                  <div class="card">
                    <div class="card-header" id="h-11">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-11" aria-expanded="false" aria-controls="c-11">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-11" class="collapse" aria-labelledby="h-11" data-parent="#faqAccordion-five">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-12">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-12" aria-expanded="false" aria-controls="c-12">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-12" class="collapse" aria-labelledby="h-12" data-parent="#faqAccordion-five">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-13">
                      <button class="acc-btn" type="button" data-toggle="collapse" data-target="#c-13" aria-expanded="true" aria-controls="c-13">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-13" class="collapse show" aria-labelledby="h-13" data-parent="#faqAccordion-five">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-14">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-14" aria-expanded="false" aria-controls="c-14">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-14" class="collapse" aria-labelledby="h-14" data-parent="#faqAccordion-five">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-15">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-15" aria-expanded="false" aria-controls="c-15">
                        <span class="text">Whre we are provided our service?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-15" class="collapse" aria-labelledby="h-15" data-parent="#faqAccordion-five">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                </div>
              </div>
              <div class="tab-pane fade" id="career" role="tabpanel" aria-labelledby="career-tab">
                <div class="accordion cmn-accordion" id="faqAccordion-six">
                  <div class="card">
                    <div class="card-header" id="h-16">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-16" aria-expanded="false" aria-controls="c-16">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-16" class="collapse" aria-labelledby="h-16" data-parent="#faqAccordion-six">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-17">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-17" aria-expanded="false" aria-controls="c-17">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-17" class="collapse" aria-labelledby="h-17" data-parent="#faqAccordion-six">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-18">
                      <button class="acc-btn" type="button" data-toggle="collapse" data-target="#c-18" aria-expanded="true" aria-controls="c-18">
                        <span class="text">What is print on demand?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-18" class="collapse show" aria-labelledby="h-18" data-parent="#faqAccordion-six">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-19">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-19" aria-expanded="false" aria-controls="c-19">
                        <span class="text">How to work?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-19" class="collapse" aria-labelledby="h-19" data-parent="#faqAccordion-six">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                  <div class="card">
                    <div class="card-header" id="h-20">
                      <button class="acc-btn collapsed" type="button" data-toggle="collapse" data-target="#c-20" aria-expanded="false" aria-controls="c-20">
                        <span class="text">Whre we are provided our service?</span>
                        <span class="plus-icon"></span>
                      </button>
                    </div>
                    <div id="c-20" class="collapse" aria-labelledby="h-20" data-parent="#faqAccordion-six">
                      <div class="card-body">
                        <p>Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec. Gravida diam eget, fames mus leo et tellus interdum, sem amet non metus nulla penatibus, aliquam nulla euismod malesuada neque ultricies, ipsum lectus cras mauris. Sociis elit est proin laoreet, ut et pulvinar curabitur inceptos est, enim ante ultrices malesuada repellendus at, consectetuer libero sem donec.</p>
                      </div>
                    </div>
                  </div><!-- card end -->
                </div>
              </div>
            </div>
          </div>
          <div class="question-form-wrapper mt-80">
            <h3 class="title">Asked your Question</h3>
            <div class="form-wrapper">
              <form class="question-form">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="faq_name" id="faq_name" placeholder="Name">
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" name="faq_email" id="faq_email" placeholder="Email">
                  </div>
                  <div class="form-group col-lg-12">
                    <textarea name="faq_message" id="faq_message" placeholder="Write your question?"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">asked question</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- faq-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection