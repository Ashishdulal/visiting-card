@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Privacy & policy</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li>Privacy</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- privacy-section start -->
  <section class="privacy-section pt-150 pb-150">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="privacy-wrapper">
            <h3 class="title">Overview</h3>
            <p>Nullam turpis dolorem, vivamus placerat in donec est, nonummy tristique massa a, dui felis in. Nulllorem aenean montes varius massula sed, ut at ante, quis velit, at amet amet placerat. Vestibuluin porttitsum eros vel in, imperdiet amet aliquam et purus dictum. In nunc nulla orci. A mascatiam iure ulum, blandit ullamcorper, justo at aenean pulvinar. Felis aliquam hac duis et feugiat eu. Duis ugiat volutpat bibendum, eget a nisl est eros, nibh metus placerat faucibus penatibus, natoque diam.</p>

            <h3 class="title">Consent and Information Collection and Use</h3>
            <p>Rictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urna dictum congue penatibus suspendissjusto, eget adipiscing, eros in donec ligula cursus integer. Accumsan estas turpis pagna debitis, placerat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauris, phasummodo augue. A sodales venenatis, amet massa fringilla, euismod elit tellus amet. Cmodo molestie dolor amet imperdiet feugia nimacus euduis.</p>
            <p>Eictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urdicongbus suspendissjusto, eget adipiscing, eros in donec ligula cursus integer. Accumsan egestas tpurgna deplarat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauris, phasellus ipsum.commoaugue. A sodales venenatis, amet massa fringilla, euismod elit tellus amet. Commodo molestie dolor ametperdiet fgiat. Enim lacus eu duisest. </p>

            <h3 class="title">Cookies and Log Files</h3>
            <p>Dictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urna dingen pebus suspendissjusto, eget adipiscing, eros in donec ligula cursus integer. Accumsan egestas tpmagna debitis plarat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauris, phasellus ipsum.commodo augue. A sodales venenatis, amet massa fringilla, euismod elit tellus amet. Comdoostie dolor amet imperdiet feugiat. Enim lacus eu duis est. Risus gravida eget, consequat tortor, felis elit dolor mauris purus pellentesque augue, leo nisl dis vehicula, vehicula magna.</p>
            <ul>
              <li>Metus taciti quis suscipit, velit dolor integer nec et magna, donec lorem faucibus duis vestilumnec luctus feugiat suspendisse nibh eleifend. Quis consectetuer, risus hendrerit dolor enim occaecat ornare molestie blandit.</li>
              <li>Quis consectetuer, risus hendrerit dolor enim, occaecat ornare molestie blandit vel rhonpeentesque. aliquam vestibulum </li>
              <li>Commodo massa sit, magna ac tempor, vitae donec adipiscing suspendisse pharetra ante in, tellus morbi aliquam world class over view company</li>
              <li>Tincidunt. Commodo cubilia fusce dictumst nulla ut pellentesque, convallis dolor orci in at, ipsum nibh erat nam morbi. Faucibus interdum, at orci, sed orci, cras mollis arcu bibendum</li>
              <li>Fames tellus mauris blandit cras at ac, mauris ipsum, tempor velit, at proident. olutpat euismod velit in ac quis metus, facilisis orci placerat, ut vitae sagittis magnis curabitur</li>
              <li>Donec placerat vivamus diam dui leo tortor, ut ac senectus class consectetuer faucibus, sed lobtis odio aliquam about the themes and over come.</li>
            </ul>

            <h3 class="title">Data Security and Retention</h3>
            <p>Aictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urna dictum congue penatibus suspendisjusget adipiscing, eros in donec ligula cursus integer. Accumsan egestatpis purusebiti placerat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauri phellus ipsum.modo augue. A sodales venenatis, amet massa fringilla, euismod elit tellus amet. Commoolestie dolor amemrdiet feugiat. Enim lacus eu duisest.</p>

            <h3 class="title">Information Form Customer</h3>
            <p>Aictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urna dictum congue penatibus suspendisjusget adipiscing, eros in donec ligula cursus integer. Accumsan egestas turpis purusebiti placerat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauris, phasellus ipsum.modo augue. A sodales venenatis, amet massa fringilla, euismod elit tellus amet.odo molestie dolor amemrdiet feugiat. Enim lacus eu duis est. Risus gravida eget, consequat tortor, felis elit dolor mauris purus pellentesque augue, leo nisl dis vehicula, vehicula magna. </p>

            <h3 class="title">Rights to your information</h3>
            <p>Aictumst molestie dui nulla bibendum tellus. Purus tincidunt amet pellentesque dis aliquet, urna dictum congue penatibus suspendisjusget adipiscing, eros in donec ligula cursus integer. Accumsan egestas turpis purusebiti placerat vestibulum commodo nascetur odio at, tortor dui posuere ornare donec mauris, phasellus ipsum.modo augue. A sodales venenatis, amet massa fringilla</p>

            <h3 class="title">Security and retention</h3>
            <p>Donec duis interdum ac, ut id lectus. Nec curabitur pede vestibulum. Et ut placerat nunc placerat craetus donec, hendrerit consequat ac turpis dictumst. Semper in hendrerit vestibulum ullamcorper. Libero urna wisi wisi, pede erat non massa, urna magna, sodales tristique tempora viverra orci morbi. Nec sunornare ac a, in porta tellus viverra eros, eius risus sed cras sit, sit ornare ante, at amet aenean est quisq  neque. Ipsum massa libero, sagittis etiam sit a morbi orci ante. Sagittis mauris etiam commodo phasellus, id mi donec pharetra velit ac, vel rutrum sed portaet.</p>

            <h3 class="title">Changes to policy</h3>
            <p>Donec duis interdum ac, ut id lectus. Nec curabitur pede vestibulum. Et ut placerat nunc placerat craetus donec, hendrerit consequat ac turpis dictumst. Semper in hendrerit vestibulum ullamcorper. Libero urna wisi wisi, pede erat non massa, urna magna, sodales tristique tempora viverra orci morbi. Nec sunornare ac a, in porta tellus viverra eros, eius risus sed cras sit, sit ornare ante, at amet aenean est quisq  neque. Ipsum massa libero, sagittis etiam sit a morbi orci ante. Sagittis mauris etiam commodo phasellus, id mi donec pharetra velit ac, vel rutrum sed portaet.</p>

            <h3 class="title">Contact us</h3>
            <p>Donec duis interdum ac, ut id lectus. Nec curabitur pede vestibulum. Et ut placerat nunc placerat craetus donec, hendrerit consequat ac turpis dictumst. Semper in hendrerit vestibulum ullamcorper. Libero urna wisi wisi, pede erat non massa, urna magna, sodales tristique tempora viverra orci morbi. Nec sunornare ac a, in porta tellus viverra eros, eius risus sed cras sit, sit ornare ante, at amet aenean est quisq  neque. Ipsum massa libero, sagittis etiam sit a morbi orci ante. Sagittis mauris etiam commodo phasellus, id mi donec pharetra velit ac, vel rutrum sed portaet.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- privacy-section end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection