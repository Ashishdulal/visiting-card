@extends('layouts.frontend.app')

@section('content')

  <!-- inner-hero-section start -->
  <section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="inner-hero-content text-center">
            <h2 class="inner-hero-title">Get in touch with us</h2>
            <ul class="page-links">
              <li><a href="index.html">Home</a></li>
              <li>Contact Us</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- inner-hero-section end -->

  <!-- contact-section start -->
  <section class="contact-section pt-150 pb-150">
    <div class="container">
      <div class="row mb-none-40">
        <div class="col-lg-4 col-md-6 mb-40">
          <div class="contact-item">
            <div class="icon"><img src="assets/images/icon/contact/1.png" alt="image"></div>
            <div class="content">
              <h3 class="title">our office location</h3>
              <p>97561 Gene Rest, North Auville CO 00498-2987</p>
            </div>
          </div><!-- contact-item end -->
        </div>
        <div class="col-lg-4 col-md-6 mb-40">
          <div class="contact-item">
            <div class="icon"><img src="assets/images/icon/contact/2.png" alt="image"></div>
            <div class="content">
              <h3 class="title">Email address</h3>
              <a href="mailto:demo1234@gmail.com">demo1234@gmail.com</a>
              <a href="mailto:demosupport@gmail.com">demosupport@gmail.com</a>
            </div>
          </div><!-- contact-item end -->
        </div>
        <div class="col-lg-4 col-md-6 mb-40">
          <div class="contact-item">
            <div class="icon"><img src="assets/images/icon/contact/3.png" alt="image"></div>
            <div class="content">
              <h3 class="title">phone number</h3>
              <a href="tel:25455454">0123 - 456 - 7890 - 2457 </a>
              <a href="tel:4455858">0123 - 456 - 7890</a>
            </div>
          </div><!-- contact-item end -->
        </div>
      </div>
      <div class="row justify-content-between mt-100">
        <div class="col-lg-6">
          <div class="contact-form-wrapper">
            <h3 class="title">Let`s talk</h3>
            <p>Aliquam at sollicitudin sit. Consequat nonummy amet eltum et fermentum purus rhoncus pellentesque luctusnulla aient. ratnulla a tellus elementumetus fringilla</p>
            <form class="contact-form mt-50">
              <div class="form-row">
                <div class="form-group col-lg-6">
                  <input type="text" name="contact_name" id="contact_name" placeholder="Name">
                </div>
                <div class="form-group col-lg-6">
                  <input type="email" name="contact_email" id="contact_email" placeholder="Email">
                </div>
                <div class="form-group col-lg-12">
                  <textarea name="contact_message" id="contact_message" placeholder="Write message"></textarea>
                </div>
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-primary">send message</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="contact-content">
            <img src="assets/images/elements/contact.png" alt="image">
            <h4 class="caption mt-20">call us today</h4>
            <a href="tel:585858" class="call-num">0123 - 456-789</a>
            <ul class="social-links">
              <li><a href="#0" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
              <li><a href="#0" class="twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#0" class="instagram"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#0" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- contact-section end -->

  <!-- map-area start -->
  <div class="map-area">
    <div class="map">
    	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28263.637663918827!2d85.31680638700367!3d27.6877946991844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a85bffffff%3A0x358c8c57ee9eb1ad!2sNepgeeks%20Technology%20-%20NTPL!5e0!3m2!1sen!2snp!4v1583131377794!5m2!1sen!2snp" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
  </div>
  <!-- map-area end -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->

@endsection