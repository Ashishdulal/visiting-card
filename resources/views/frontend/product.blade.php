@extends('layouts.frontend.app')

@section('content')

<!-- inner-hero-section start -->
<section class="inner-hero-section" style="background-image: url('assets/images/bg/inner-hero.jpg');">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="inner-hero-content text-center">
          <h2 class="inner-hero-title">Choose product</h2>
          <ul class="page-links">
            <li><a href="/">Home</a></li>
            <li>product</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- inner-hero-section end -->

<!-- shop-section start -->
<section class="shop-section pt-150 pb-120">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="product-filter-area">
          <form class="product-search-form">
            <input type="text" name="product_search" id="product_search" placeholder="Search here">
            <button onclick="callAjax(event)"><i class="flaticon-vr-glass"></i></button>
          </form>
            <!-- <div class="filter-options">
              <span class="button"><i class="fa fa-sliders"></i>Filters</span>
              <ul class="filter-list">
                <li><a href="#">Electronics</a></li>
                <li><a href="#">Mobile</a></li>
                <li><a href="#">Home Acc</a></li>
                <li><a href="#">Computer</a></li>
              </ul>
            </div> -->
          </div>
        </div>
      </div>
      <div class="row mt-50">
        <div class="col-lg-3">
          <div class="sidebar-shop">
            <div class="widget">
              <h6 class="widget-title">Categories</h6>
              <form class="filter-form">
                
                <div class="radio-wrapper">
                  <label for="shirt">
                    <input type="radio" id="shirt" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text">
                      <a href="#" onclick="callAjax(event,'all', 0)">All Products</a>
                    </span>
                  </label> 
                </div><!-- radio-wrapper end -->
                @foreach($productCat as $cat)
                <div class="radio-wrapper">
                  <label for="shirt">
                    <input type="radio" id="shirt" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text"><a href="#" onclick="callAjax(event,{{$cat->id}}, 0)">{{$cat->category_name}}</a></span>
                  </label> 
                </div><!-- radio-wrapper end -->
                @endforeach
              </form>
            </div><!-- widget end -->
            <div class="widget">
              <h6 class="widget-title">Type</h6>
              <form class="filter-form">
                @foreach($productType as $type)
                <div class="radio-wrapper">
                  <label for="type">
                    <input type="radio" id="type" name="radio-group">
                    <span class="fake-box"></span>
                    <span class="radio-text"><a href="#" onclick="callAjax(event, 0, {{$type->id}})">{{$type->product_type}}</a></span>
                  </label> 
                </div><!-- radio-wrapper end -->
                @endforeach
              </form>
            </div><!-- widget end -->

          </div>
        </div><!-- sidebar-shop end -->
        <div class="col-lg-9" id="productSearchNormal">
          <h4 class="text-header">All Products</h4>
          @foreach($productCat as $cat)
          <h4 class="text-cat-name">{{$cat->category_name}}:</h4><br>
          <div class="row">
            @foreach($products as $prod)
            @if($prod->cat_id == $cat->id)
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="product-single text-center">
                <a href="/product-details/{{$prod->id}}" class="item-link"></a>
                <div class="thumb"><img src="/uploads/product/{{$prod->product_image3}}" alt="image"></div>
                <div class="details">
                  <h3 class="product-name">{{$prod->product_name}}</h3>
                  <span class="price">Code: {{$prod->product_code}}</span>
                </div>
              </div>
            </div>
            @endif
            @endforeach<!-- product-single end -->
          </div>
          @endforeach
          
        </div>
        <div id="productSearchContent" class="col-lg-9" ></div>
      </div>
    </div>
  </section>
  <!-- shop-section end  -->

  <!-- subscribe-section start -->
  <div class="subscribe-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <h2 class="subscribe-title">Subscribe Newslatter?</h2>
          <form class="subscribe-form">
            <input type="email" name="subscribe_email" id="subscribe_email" placeholder="Email address">
            <button type="submit" class="subscribe-btn"><img src="assets/images/icon/paper-plane.png" alt="image"></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- subscribe-section end -->
  <script type="text/javascript">
    function callAjax(evt, id, type_id){
      var search = $('#product_search').val();
      evt.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: 'GET',
        beforeSend: function(){
         $('#preLoader').css("display", "block");
         $('#preLoader').css("opacity", "1");
       },
       url: '/search-category/',
       data: {'id': id ,'type_id': type_id, 'search':search},
       success: function (response) {
        $('#productSearchContent').show();
        $('#productSearchContent').html(response);
        $('#productSearchNormal').hide();

      },
      complete: function(){
        $('#preLoader').css("display", "none");
        $('#preLoader').css("opacity", "0");
      }
    });

    }
  </script>
  @endsection