@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Products</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Products</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->

		@if ($errors->any())
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					<div class="card-header">
						<a class="btn btn-primary" href="/home/product/create">Add New Product</a>
					</div>

					<div class="card-body">
						<table class="table table-responsive-xl table-hover display" id="searchFunc" style="width:100%">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Product Code</th>
									<th scope="col">Product Name</th>
									<th scope="col">Description</th>
									<th scope="col">Stock</th>
									<th scope="col">Image</th>
									<th scope="col">Category</th>
									<th scope="col">Type</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $product)
								<tr>
									<th>{{$loop->iteration}}</th>
									<th>{{$product->product_code}}</th>
									<td>{{$product->product_name}}</td>
									<td><?php
									$excerpt = $product->product_description;
				                    $the_str = substr($excerpt, 0, 100);
				                    echo $the_str.'...';
									 ?>
									 </td>
									<td>{{$product->product_stock}}</td>
									<td>
										<div class="row">
											<div class="col-sm-4">
												<img class="preload-image" src="/uploads/product/{{$product->product_image1}}" alt="{{$product->product_name}}">
											</div>
											<div class="col-sm-4">
												<img class="preload-image" src="/uploads/product/{{$product->product_image2}}" alt="{{$product->product_name}}">
											</div>
											<div class="col-sm-8 text-center">
												<img class="preload-image" src="/uploads/product/{{$product->product_image3}}" alt="{{$product->product_name}}">
											</div>
										</div>
									</td>
									<td>{{$product->category_name}}</td>
									<td>{{$product->product_type}}</td>
									<td class="text-center"><a class="btn btn-primary size-btn" href="/home/product/edit/{{$product->id}}">Edit</a> | 
										<form class="delete-box" action="/home/product/delete/{{$product->id}}" method="post">
											@csrf
											{{ method_field('delete') }}
											<button type="submit" class="btn btn-danger" onclick="confirmation(event)">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>							

				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	function confirmation(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>
@endsection