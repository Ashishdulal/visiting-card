@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Add Product</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Add Product</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div style="margin: 0 auto;" class="col-md-8">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Add New Product</h3>
					</div>

					<div class="card-body">
						<form method="POST" action="/home/product/create" enctype="multipart/form-data">
							@csrf
								  <div class="form-group row">
								  	<div class="col-sm-6">
									<label for="cat_id">Product Category</label>
									<select name="cat_id" class="form-control">
										<option>---</option>
										@foreach($pCat as $cat)
										<option value="{{$cat->id}}">{{$cat->category_name}}</option>
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the Category to describe the product.</small>
								  </div>
								  <div class="col-sm-6">
									<label for="type_id">Product Type</label>
									<select name="type_id" class="form-control">
										<option>---</option>
										@foreach($type as $typ)
										<option value="{{$typ->id}}">{{$typ->product_type}}</option>
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the Type to describe the product.</small>
								  </div>
								</div>
								  <div class="form-group">
									<label for="Product_name">Product Name</label>
									<input type="text" class="form-control" name="product_name" id="Product_name" aria-describedby="role" placeholder="Enter Product Name" required>
									<small id="role" class="form-text text-muted">Enter the product name to describe the product.</small>
								  </div>
								  <div class="form-group">
								  	<div class="row">
								  		<div class="col-sm-4">
								  			<input type="file" name="product_image1" onchange="readURL(this);" data-id="productImageFirst" accept="image/png, image/jpg, image/jpeg" class="form-control-file">
								  			<img src="/uploads/product/default-image.png" class="preload-image" id="productImageFirst">
								  		</div>
								  		<div class="col-sm-4">
								  			<input type="file" onchange="readURL(this);" data-id="productImage2" accept="image/png, image/jpg, image/jpeg" name="product_image2" class="form-control-file">
								  			<img src="/uploads/product/default-image.png" class="preload-image" id="productImage2">
								  		</div>
								  		<div class="col-sm-4">
								  			<input type="file" onchange="readURL(this);" data-id="productImage3" accept="image/png, image/jpg, image/jpeg" name="product_image3" class="form-control-file">
								  			<img src="/uploads/product/default-image.png" class="preload-image" id="productImage3">
								  		</div>
								  	</div>
								  </div>
								  <div class="form-group row">
								  	<div class="col-sm-6">
									<label for="pstock">Product Stock</label>
									<input type="number" class="form-control" name="product_stock" id="pstock" aria-describedby="pstock" placeholder="Enter Number" required>
								  </div>
								  <div class="col-sm-6">
									<label for="product_code">Product code</label>
									<input type="text" class="form-control" name="product_code" id="product_code" aria-describedby="product_code" placeholder="Enter the required product code" required>
								  </div>
								</div>
								  <div class="form-group">
									<label for="description">Product Description</label>
									<textarea class="form-control ckeditor" name="product_description"></textarea>
								</div>
								  <button type="submit" class="btn btn-primary">Submit</button>
								</form>

					</div>														
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>

@endsection