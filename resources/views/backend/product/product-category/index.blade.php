@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Category</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Category</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->

		@if ($errors->any())
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					<div class="card-header">
						<a class="btn btn-primary" href="/home/product-category/create">Add new category</a>
					</div>

					<div class="card-body">

						<table class="table table-responsive-xl table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Category Name</th>
									<th scope="col">Slug</th>
									<th scope="col" class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($allcat as $cat)
								<tr>
									<th scope="row">{{$loop->iteration}}</th>
									<td>{{$cat->category_name}}</td>
									<td>{{$cat->category_slug}}</td>
									<td class="text-center"><a class="btn btn-primary" href="/home/product-category/edit/{{$cat->id}}">Edit</a> | 
										<form class="delete-box" action="/home/product-category/delete/{{$cat->id}}" method="post">
											@csrf
											{{ method_field('delete') }}
											<button type="submit" class="btn btn-danger" onclick="confirmation(event)">Delete</button>
										</form>
									</td>
								</tr>
								<div class="under-table">
							<thead class="under-table-row">

								<tr>
									<th>Quantity</th>
									<th>Thickness</th>
									<th>Sides</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody class="under-table-col">

								@foreach($pVar as $var)
								@if($var->category_id == $cat->id)
								<tr>  
									<td>{{$var->product_quantity}}</td> 
									<td>{{$var->product_thickness}}</td>  
									<td>{{$var->product_sides}}</td>  
									<td>{{$var->product_price}}</td> 
								</tr>
								@endif
								@endforeach
								<tr><td style="border-top: none;" colspan="6"><br><br></td></tr>
							</tbody>
						</div>
								@endforeach
							</tbody>
						</table>

					</div>							
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	function confirmation(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>
@endsection