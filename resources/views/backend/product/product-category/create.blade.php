@extends('layouts.backend.app')
<script src="https://code.jquery.com/jquery-3.3.1.min.js" type='text/javascript'></script>

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Add Product Category</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Add Product Category</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		@if ($errors->any())
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div style="margin: 0 auto;" class="col-md-10">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Add New Category</h3>
					</div>

					<div class="card-body">
						<form method="POST" action="/home/product-category/create">
							@csrf
							<div class="form-group">
								<label for="category_name">Category Name</label>
								<input type="text" class="form-control" name="category_name" id="category_name" aria-describedby="category" placeholder="Enter Category Name" required>
								<small id="category" class="form-text text-muted">Enter the category name to describe the category.</small>
							</div>
							<div class="form-group">
								<label for="slug">Slug</label>
								<input type="text" class="form-control" name="category_slug" id="slug" aria-describedby="slug" placeholder="Enter Slug">
							</div>
							<table class="table table-bordered" id="dynamicTable">  
								<tr>
									<label>Product</label>
									<th>Quantity</th>
									<th>Thickness</th>
									<th>Sides</th>
									<th>Price</th>
									<th>Action</th>
								</tr>
								<tr>  
									<td><input type="text" required name="product_quantity[]" placeholder="Enter the Product Quantity" class="form-control" /></td> 
									<td><input type="number" name="product_thickness[]" min="0" placeholder="Enter product Thickness" class="form-control" /></td>  
									<td><input type="number" name="product_sides[]" min="0" placeholder="Enter product Sides" class="form-control" /></td>  
									<td><input type="number" name="product_price[]" min="0" placeholder="Enter product Price" class="form-control" /></td>  
									<td><button type="button" name="add" id="add" class="btn btn-primary">Add More</button></td>  
								</tr>  
							</table>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>

					</div>														
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">

	var i = 0;

	$("#add").click(function(){

                        // alert('button clicked');

                        ++i;

                        $("#dynamicTable").append('<tr><td><input type="text" required name="product_quantity[]" placeholder="Enter the Product Quantity" class="form-control" /></td><td><input type="number" name="product_thickness[]" min="0" placeholder="Enter product Thickness" class="form-control" /></td><td><input type="number" name="product_sides[]" min="0" placeholder="Enter product Sides" class="form-control" /></td><td><input type="number" name="product_price[]" min="0" placeholder="Enter product Price" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
                    });

	$(document).on('click', '.remove-tr', function(){  
		$(this).parents('tr').remove();
	});  

</script>
@endsection