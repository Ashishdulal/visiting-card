@extends('layouts.backend.app')

@section('content')
<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Blog Category</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Blog Category</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div style="margin: 0 auto;" class="col-md-6">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Add New Category</h3>
					</div>

					<div class="card-body">
			<form  method="POST" action={{route('make.blog')}}  class="ng-untouched ng-pristine ng-valid">

				{{csrf_field()}}

				<label for="title" >News Category Name</label>
				<div  class="form-group">
					<input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ old('title') }}" placeholder="Enter the Blog title">
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>			
		</div>
	</div>
</div>
</div>
</div><!--/.col-->

@endsection