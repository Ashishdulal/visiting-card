@extends('layouts.backend.app')

@section('content')
<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Blog Category</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Blog Category</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					<div class="card-header">
						<a class="btn btn-primary" href="/home/blog-category/create">Add New Blog Category</a>
					</div>

					<div class="card-body">
					<table class="table table-borderless table-data3">
						<table class="table table-hover">
							<tr>
								<th>S. NO.</th>
								<th>News Category</th>
								<th>Action</th>
							</tr>

							@foreach ($blogcategories as $BlogCategory)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{$BlogCategory->title}}</td>
								<td ><a href="/home/blog-category/edit/{{$BlogCategory->id}}"> <button class="btn btn-info size-btn">Edit</button></a> |
									<form method="delete" action="{{route('delete.blog',$BlogCategory->id)}}"> 

										{{csrf_field()}}

										{{method_field('DELETE')}}
										<div class="field"><button class="btn btn-danger" onclick="makeWarning(event)">Delete</button></div>
									</form>

								</td>
							</tr>
							@endforeach
						</table>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
		</div><!--/.col-->
		<script type="text/javascript">
			function makeWarning(evt){
				let result = confirm("Are you sure to Delete?");
				if(! result){
					evt.stopPropagation();
					evt.preventDefault();	
				}
			}
		</script>
		@endsection