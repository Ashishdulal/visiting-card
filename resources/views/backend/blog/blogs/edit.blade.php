@extends('layouts.backend.app')

@section('content')
<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Edit Blog</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Edit Blog</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-10">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Edit Blog</h3>
					</div>

					<div class="card-body">
			<form method="POST" action="/home/blog/edit/{{ $blogs->id }}" class="form-group" enctype="multipart/form-data">

				{{csrf_field()}}

				<div class="form-group">
					<label for="title">
						Edit The Post Title:
					</label>
					<input type="text" name="title" required class="form-control" value="{{$blogs->title}}">
				</div>
				<div class="form-group">
					<label for="cat_id">
						Choose The Post Category:
					</label>
					<select name="cat_id" class="form-control">


						@foreach($blogcategories as $Postcat)
						@if($Postcat->id == $blogs->cat_id)
						<option value="{{$Postcat->id}}">{{$Postcat->title}}</option>
						@endif
						@endforeach


						@foreach($blogcategories as $Postcat)
						@if($Postcat->id != $blogs->cat_id)
						<option value="{{$Postcat->id}}">{{$Postcat->title}}</option>
						@endif
						@endforeach
					</select>
				</div>
				<!-- image input-->
				<div class="form-group">
					<label for="image">Select The Post Image:</label><br>
						<span style="margin: 0 0 20px 10px; " class="au-breadcrumb-span">[ Note: Please upload the image size 800*800 and should be less than 100kb ]</span>
					<div class="row">
						<div  class="col-md-6">
							<app-image-upload  >
								<div  class="fileinput text-center">
									<div  class="thumbnail img-raised process">
										<img  alt="..." class="preload-image" src="/uploads/blogs/{{$blogs->f_image}}" id="blog-image1">
									</div>
									<input  type="file" name="f_image" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" data-id="blog-image1">
									</div>
								</app-image-upload>
							</div>
							<div  class="col-md-6">
								<app-image-upload  >
									<div  class="fileinput text-center process">
										<div  class="thumbnail img-raised">
											<img  alt="..." class="preload-image" src="/uploads/blogs/{{$blogs->i_image}}" id="blog-image2">
										</div>
										<input  type="file" name="i_image" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" data-id="blog-image2">
										</div>
									</app-image-upload>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="image">Description:</label>
							<div class="col-md-9">
								<textarea id="description" name="description" type="text" required="" class="form-control ckeditor"> {{$blogs->description}}</textarea>
							</div>
						</div>


						<div class="form-group">
							<button type="submit" class="btn btn-primary">Update Post</button>
						</div>
					</form>
				</div>
				<div class="card-footer">

				</div>
			</div>
		</div>



	</div><!--/.col-->

	@endsection