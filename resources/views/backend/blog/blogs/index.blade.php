@extends('layouts.backend.app')

@section('content')
<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Blog</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Blog</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>

				<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					<div class="card-header">
						<a class="btn btn-primary" href="/home/blog/create">Add New Blog</a>
					</div>

					<div class="card-body">
						<table class="table table-responsive-xl table-hover display" id="searchFunc" style="width:100%">

							<thead>
								<tr>
									<th>Id</th>
									<th>Title</th>
									<th>Description</th>
									<th>Images</th>
									<th>Category</th>
									<th>Action</th>
									<th>Author</th>
								</tr>
							</thead>
							<tbody>
								@foreach($blogs as $blog)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{$blog->title}}</td>
									<td><?php echo ($blog->description)?></td>
									<td class="work-img process">
										<img src="/uploads/blogs/{{$blog->f_image}}" class="preload-image" alt="{{$blog->title}}">
										<img src="/uploads/blogs/{{$blog->i_image}}" class="preload-image" alt="{{$blog->title}}">
									</td>
									<td>@foreach ($blogcategories as $blog_cat)
								@if( $blog_cat->id == $blog->cat_id)
									{{$blog_cat->title}}
								@endif
						@endforeach
									</td>
									<td>
										<a href="/home/blog/edit/{{$blog->id}}" class="btn btn-primary size-btn">
											Edit
										</a>|
										<form class="delete-box" action="{{route('delete-post.blog',$blog->id)}}">
											<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
										</form>
									</td>
									<td><p>Admin</p></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- END DATA TABLE-->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection