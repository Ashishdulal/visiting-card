@extends('layouts.backend.app')

@section('content')
<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Blogs</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Blogs</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-10">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Add New Blog</h3>
					</div>

					<div class="card-body">
			<form class="form-horizontal" action="/home/blog/create" method="POST" enctype="multipart/form-data">
				@csrf
				<fieldset>
					<!-- Post Title input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="title">Post Title:</label>
						<div class="col-md-9">
							<input id="title" name="title" type="text" required="" placeholder="Enter the Post Title" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Choose Post Category:</label>
						<div class="col-md-9">
							<select name="cat_id" required class="form-control">

								<option value="">---</option>
								@foreach($blogcategories as $blogcategory)

								<option value="{{$blogcategory->id}}">{{$blogcategory->title}}</option>

								@endforeach
							</select>
						</div>
					</div>

					<!-- image input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Select The Product Image:</label><br>
						<span style="margin: 0 0 20px 10px; " class="au-breadcrumb-span">[ Note: Please upload the image size 800*800 and should be less than 100kb ]</span>
						<div class="row">
							<div  class="col-md-4">
								<app-image-upload  >
									<div  class="fileinput text-center">
										<input  type="file" onchange="readURL(this);" name="f_image"  accept="image/png, image/jpg, image/jpeg" data-id="blog-image1">
										<img src="/uploads/blogs/blog_default.png" class="preload-image" id="blog-image1">
									</div>
								</app-image-upload>
							</div>
							<div  class="col-md-4">
								<app-image-upload  >
									<div  class="fileinput text-center">
										<input  type="file" onchange="readURL(this);" name="i_image"  accept="image/png, image/jpg, image/jpeg" data-id="blog-image2">
										<img src="/uploads/blogs/blog_default.png" class="preload-image" id="blog-image2">
									</div>
								</app-image-upload>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Description:</label>
						<div class="col-md-9">
							<textarea id="description" name="description" type="text" required="" placeholder="Post description" class="form-control ckeditor"></textarea>
						</div>
					</div>

					<!-- Form actions -->
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Submit
					</button>
					<button type="reset" class="btn btn-danger btn-sm">
						<i class="fa fa-ban"></i> Reset
					</button>
				</fieldset>
			</form>
		</div>
		<div class="card-footer">

		</div>
	</div>
</div>



</div><!--/.col-->

@endsection