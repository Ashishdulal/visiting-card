@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Users</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Users</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div style="margin: 0 auto;" class="col-md-6">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-user"></i> Add New User</h3>
					</div>

					<div class="card-body">
						<form method="POST" action="{{ route('register') }}">
							@csrf
							<input type="hidden" name="image" value="admin-image.png">
							<div class="form-group row">
								<label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>

								<div class="col-md-12">
									<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

									@error('name')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									@enderror
								</div>
							</div>

							<div class="form-group row">
								<label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

								<div class="col-md-12">
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									@enderror
								</div>
							</div>
								<div class="form-group">
									<label for="email" class="col-md-12 col-form-label">{{ __('Roles') }}</label>
								<select class="form-control" name="role_id">
									<option>---</option>
									@foreach($roles as $role)
								  <option value="{{$role->id}}">{{$role->role_name}}</option>
								  @endforeach
								</select>
								</div>
							<div class="form-group row">
								<label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

								<div class="col-md-12">
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

									@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									@enderror
								</div>
							</div>

							<div class="form-group row">
								<label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>

								<div class="col-md-12">
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md">
									<button type="submit" class="btn btn-primary">
										{{ __('Register') }}
									</button>
								</div>
							</div>
						</form>

					</div>														
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>

@endsection