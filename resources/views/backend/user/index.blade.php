@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Users</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Users</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->

				@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					@if(Auth::user()->hasRole('admin'))
					<div class="card-header">
						<a class="btn btn-primary" href="/home/user/add">Add new User</a>
					</div>
					@endif

					<div class="card-body">

						<table class="table table-responsive-xl table-hover display" id="searchFunc" style="width:100%">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Name</th>
									<th scope="col">Image</th>
									<th scope="col">Email</th>
									<th scope="col">Role</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
							@if(Auth::user()->hasRole('admin'))
								@foreach($users as $user)
								<tr>
									<th scope="row">{{$loop->iteration}}</th>
									<td>{{$user->name}}</td>
									<td class="index-main-img"><img class="index-image" src="/uploads/user/{{$user->image}}"></td>
									<td>{{$user->email}}</td>
									<td>
										<ul style="padding-left: 10px;list-style: none";>
										@foreach($userRoles as $role)
										@if($user->id == $role->user_id)
											<li>{{$role ->role_name}}</li>
											@endif
										@endforeach
										</ul>
									</td>
									<td><a class="btn btn-primary" href="/home/user/edit/{{$user->id}}">Edit</a> | 
										<form class="delete-box" action="/home/user/delete/{{$user->id}}" method="post">
											@csrf
											{{ method_field('delete') }}
											<button type="submit" class="btn btn-danger" onclick="confirmation(event)">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
								@else
								<tr>
									<th scope="row">-</th>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>
										<ul style="padding-left: 10px;list-style: none;">
										@foreach($userRoles as $role)
										@if($user->id == $role->user_id)
											<li>{{$role ->role_name}}</li>
											@endif
										@endforeach
										</ul>
									</td>
									<td><a class="btn btn-primary" href="/home/auth-user/edit/{{$user->id}}">Edit</a>
									</td>
								</tr>
								@endif
							</tbody>
						</table>

					</div>							
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	function confirmation(evt){
		let result = confirm("Are you sure to delete this user ?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>
@endsection