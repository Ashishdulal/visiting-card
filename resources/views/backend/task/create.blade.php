@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">Add new Task</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Add new Task</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
						@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
		<!-- end row -->
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div style="margin: 0 auto;" class="col-md-6">						
				<div class="card mb-3">
					<div class="card-header">
						<h3><i class="fa fa-tasks"></i> Add New Task</h3>
					</div>

					<div class="card-body">
						<form method="POST" action="/home/task/create">
							@csrf
							<input type="hidden" name="user_id" value="{{$userId}}">
								  <div class="form-group">
									<label for="task_name">Task Name</label>
									<input type="text" class="form-control" name="task_name" id="task_name" aria-describedby="task" placeholder="Enter Task Name" required>
									<small id="task" class="form-text text-muted">Enter the task name to describe the task.</small>
								  </div>
								  <div class="form-group">
									<label for="task_description">Task Description</label>
									<textarea class="form-control ckeditor" name="task_description"></textarea>
									<small id="task" class="form-text text-muted">Add description to describe the task.</small>
								  </div>
								  <div class="form-group">
									<label for="payment_status">Payment Status</label>
									<select name="payment_status" class="form-control">
										<option>---</option>
										@foreach($paymentStat as $pStat)
										
										<option value="{{$pStat->payment_status_name}}">{{$pStat->payment_status_name}}</option>
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the Payment Status of the task.</small>
								  </div>
								  <div class="form-group">
									<label for="work_status">Work Status</label>
									<select name="work_status" class="form-control">
										<option>---</option>
										@foreach($workStat as $wStat)
										<option value="{{$wStat->status_name}}">{{$wStat->status_name}}</option>
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the work Status of the task.</small>
								  </div>
								  <div class="form-group">
									<label for="user_id">Assign To</label>
									<select name="user_id" class="form-control">
										<option>---</option>
										@foreach($developers as $dev)
										@if($dev->role_id == 3)
										<option value="{{$dev->id}}">{{$dev->name}}</option>
										@endif
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the Developer to assign the task.</small>
								  </div>
								  <div class="form-group">
									<label for="client_id">Work from User</label>
									<select name="client_id" class="form-control">
										<option>---</option>
										@foreach($developers as $user)
										@if($user->role_id == 5)
										<option value="{{$user->id}}">{{$user->name}}</option>
										@endif
										@endforeach
									</select>
									<small id="role" class="form-text text-muted">Select the client for the task to be done.</small>
								  </div>
								  <div class="form-group">
									<label for="comments">Client comments on the task</label>
									<textarea class="form-control ckeditor" rows="5" name="comments" id="comments" required>
									</textarea>
								  </div>
								  <button type="submit" class="btn btn-primary">Submit</button>
								</form>

					</div>														
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>

@endsection