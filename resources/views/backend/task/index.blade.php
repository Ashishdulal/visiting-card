@extends('layouts.backend.app')

@section('content')

<!-- Start content -->
<div class="content">

	<div class="container-fluid">


		<div class="row">
			<div class="col-xl-12">
				<div class="breadcrumb-holder">
					<h1 class="main-title float-left">All Task</h1>
					<ol class="breadcrumb float-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">All Task</li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->

		@if ($errors->any())
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		@if (Session::has('success'))
		<div class="alert alert-success text-center">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<p>{{ Session::get('success') }}</p>
		</div>
		@endif
		<div class="alert alert-success" role="alert">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="row">
			
			<div class="col-md-12">						
				<div class="card mb-3">
					<div class="card-header">
						<a class="btn btn-primary" href="/home/task/create">Add New Task</a>
					</div>

					<div class="card-body">

						<table class="table table-responsive-xl table-hover display" id="searchFunc" style="width:100%">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Task Name</th>
									<th scope="col">Task Description</th>
									<th scope="col">User Assigned</th>
									<th scope="col">Client Name</th>
									<th scope="col">Work Status</th>
									<th scope="col">Payment Status</th>
									<th>Assigned To</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($tasks as $task)
								<tr>
									<th scope="row">{{$loop->iteration}}</th>
									<td>{{$task->task_name}}</td>
									<td><?php echo ($task->task_description) ?></td>
									<td>
										{{$task->user_id}}
									</td>
									<td>
										@foreach($userSelected as $asstask)
										@if($asstask->task_id == $task->id)
											{{$asstask->name}}
										@endif
										@endforeach
									</td>
									<td>{{$task->work_status}}</td>
									<td>{{$task->payment_status}}</td>
									<td>
										@foreach($assignedTasks as $asstask)
										@if($asstask->task_id == $task->id)
										{{$asstask->name}}
										@endif
										@endforeach
									</td>
									<td><a class="btn btn-primary size-btn" href="/home/task/edit/{{$task->id}}">Edit</a> | 
										<form class="delete-box" action="/home/task/delete/{{$task->id}}" method="post">
											@csrf
											{{ method_field('delete') }}
											<button type="submit" class="btn btn-danger" onclick="confirmation(event)">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>

					</div>							
				</div><!-- end card-->					
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	function confirmation(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>
@endsection