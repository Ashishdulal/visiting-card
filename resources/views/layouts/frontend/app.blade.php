
<!-- meta tags and other links -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nepgeeks - Print In Your Hand</title>
  <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" sizes="16x16">
  <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/lightcase.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/chart.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
</head>
<body>
  <div class="dark-version">
    <!-- header section  -->
    <div class="preloader" id="preLoader">
      <div class="preloader-box">
        <div>लो</div>
        <div>ड</div>
        <div> </div>
        <div>हुँ</div>
        <div>दै</div>
        <div>छ</div>
        <div>...</div>
      </div>
    </div>

@include('layouts.frontend.header')

@yield('content')

@include('layouts.frontend.footer')
  </div>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="{{asset('assets/js/vendor/jquery.nice-select.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/slick.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/wow.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/lightcase.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.countdown.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.easing.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/TweenMax.min.js')}}"></script>
  <script src="{{asset('assets/js/vendor/jquery.paroller.min.js')}}"></script>
  <script src="{{asset('assets/js/contact.js')}}"></script>
  <script src="{{asset('assets/js/vendor/tilt.jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>