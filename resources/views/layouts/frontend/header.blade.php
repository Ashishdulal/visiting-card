    <!-- header-section start  -->
    <header class="header-section">
      <div class="header-bottom">
        <div class="container">
          <nav class="navbar navbar-expand-xl align-items-center">
            <a class="site-logo site-title" href="/"><img src="{{asset('assets/images/logo.png')}}" alt="site-logo"><span class="logo-icon"><i class="flaticon-fire"></i></span></a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="menu-toggle"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav main-menu ml-auto">
                <li><a href="/">Home</a></li>
                <li><a href="/about">about</a></li>
                <li class="menu_has_children"><a href="/#0">product</a>
                  <ul class="sub-menu">
                    <li><a href="/product">Shop page</a></li>
                    <li><a href="/product-details">Shop Details</a></li>
                    <li><a href="/custom-design">Custom Design</a></li>
                    <li><a href="/cart">Cart Page</a></li>
                    <li><a href="/checkout">Checkout</a></li>
                  </ul>
                </li>
                <li class="menu_has_children"><a href="/#0">Page</a>
                  <ul class="sub-menu">
                    <li><a href="/privacy">Privacy</a></li>
                    <li><a href="/faq">FAQ</a></li>
                    <li><a href="/error-404">Error Page</a></li>
                  </ul>
                </li>
                <li class="menu_has_children"><a href="/#0">Blog</a>
                  <ul class="sub-menu">
                    <li><a href="/blogs">Blog</a></li>
                    <li><a href="/blog-details">Blog Details</a></li>
                  </ul>
                </li>
                <li><a href="/contact">contact</a></li>
              </ul>
              <div class="nav-right d-flex align-items-center">
                <div class="header-cart">
                  <button class="header-cart-btn">
                    <i class="flaticon-commerce-and-shopping"></i>
                    <span class="header-cart-total-num">02</span>
                  </button>
                  <div class="header-cart-area">
                    <ul class="header-cart-list">
                      <li class="single-product">
                        <div class="product-thumb"><img src="{{asset('assets/images/product/t1.png')}}" alt="image"></div>
                        <div class="product-details">
                          <h5 class="product-name">Men T-shirt</h5>
                          <span class="product-amount">$75 x 2</span>
                        </div>
                        <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                      </li><!-- single-product end -->
                      <li class="single-product">
                        <div class="product-thumb"><img src="{{asset('assets/images/product/t2.png')}}" alt="image"></div>
                        <div class="product-details">
                          <h5 class="product-name">Custom Mug</h5>
                          <span class="product-amount">$25 x 2</span>
                        </div>
                        <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                      </li><!-- single-product end -->
                      <li class="single-product">
                        <div class="product-thumb"><img src="{{asset('assets/images/product/t3.png')}}" alt="image"></div>
                        <div class="product-details">
                          <h5 class="product-name">Hoodie</h5>
                          <span class="product-amount">$95 x 2</span>
                        </div>
                        <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                      </li><!-- single-product end -->
                    </ul>
                    <div class="cart-btn-area">
                      <a href="/#0">view cart</a>
                      <a href="/#0">checkout</a>
                    </div>
                  </div>
                </div>
                <ul class="nav-right-menu">
                  <li><a href="/#0" class="signup-open-btn">sign up</a></li>
                  <li><a href="/#0" class="login-open-btn">sign in</a></li>
                </ul>
              </div>
            </div><!-- navbar-collapse end -->
            <div class="header-cart">
              <button class="header-cart-btn">
                <i class="flaticon-commerce-and-shopping"></i>
                <span class="header-cart-total-num">02</span>
              </button>
              <div class="header-cart-area">
                <ul class="header-cart-list">
                  <li class="single-product">
                    <div class="product-thumb"><img src="assets/images/product/t1.png" alt="image"></div>
                    <div class="product-details">
                      <h5 class="product-name">Men T-shirt</h5>
                      <span class="product-amount">$75 x 2</span>
                    </div>
                    <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                  </li><!-- single-product end -->
                  <li class="single-product">
                    <div class="product-thumb"><img src="assets/images/product/t2.png" alt="image"></div>
                    <div class="product-details">
                      <h5 class="product-name">Custom Mug</h5>
                      <span class="product-amount">$25 x 2</span>
                    </div>
                    <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                  </li><!-- single-product end -->
                  <li class="single-product">
                    <div class="product-thumb"><img src="assets/images/product/t3.png" alt="image"></div>
                    <div class="product-details">
                      <h5 class="product-name">Hoodie</h5>
                      <span class="product-amount">$95 x 2</span>
                    </div>
                    <button type="button" class="cart-item-delete-btn"><i class="fa fa-trash-o"></i></button>
                  </li><!-- single-product end -->
                </ul>
                <div class="cart-btn-area">
                  <a href="/#0">view cart</a>
                  <a href="/#0">checkout</a>
                </div>
              </div>
            </div><!-- header-cart end -->
          </nav>
        </div>
      </div>
    </header>
    <!-- header-section end  -->

    <!-- login-section start -->
<div class="login-section">
  <div class="section-bg-image" style="background-image: url('assets/images/bg/inner-hero.jpg');"></div>
  <div class="login-area">
    <div class="login-close"></div>
    <div class="login-wrapper">
      <div class="text-center">
        <h3 class="title text-center">Hi, welcome to Nepgeeks</h3>
        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibusnon</p>
      </div>
      <form class="signin-form">
        <div class="form-row justify-content-between">
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="login_email" id="login_email" placeholder="Email or User Name">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="login_pass" id="login_pass" placeholder="Password">
          </div>
          <div class="col-lg-12 text-center mt-4">
            <a href="/#0" class="btn btn-primary">Signin Now</a>
          </div>
        </div>
      </form>
      <div class="form-footer text-center mt-20">
        <a href="/#0" class="forget-pass">Forget my password</a>
        <span class="mt-40">Signin with</span>
        <ul class="d-flex justify-content-center">
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/google.png" alt="image"></a></li>
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/facebook.png" alt="image"></a></li>
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/twitter.png" alt="image"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- login-section end -->

<!-- signup-section start -->
<div class="signup-section">
  <div class="section-bg-image" style="background-image: url('assets/images/bg/inner-hero.jpg');"></div>
  <div class="signup-area">
    <div class="signup-close">
    </div>
    <div class="signup-wrapper">
      <div class="text-center">
        <h3 class="title text-center">Hi, welcome to Nepgeeks</h3>
        <p>Pellentesque lobortis pharetra, interdum pellentesque nunc ipsum fusce dapibusnon</p>
      </div>
      <form class="signup-form">
        <div class="form-row justify-content-between">
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_email" id="signup_email" placeholder="Email or User Name">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_pass" id="signup_pass" placeholder="Password">
          </div>
          <div class="col-lg-12 custom-form-field">
            <i class="fa fa-user"></i>
            <input type="text" name="signup_re_pass" id="signup_re_pass" placeholder="Re-password">
          </div>
          <div class="col-lg-12 text-center mt-4">
            <a href="/#0" class="btn btn-primary">Signup Now</a>
          </div>
        </div>
      </form>
      <div class="form-footer text-center mt-20">
        <p>Already i have an account in here <a href="/#0">Signin</a></p>
        <span class="mt-40">Signin with</span>
        <ul class="d-flex justify-content-center">
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/google.png" alt="image"></a></li>
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/facebook.png" alt="image"></a></li>
          <li><a href="/#0"><img src="assets/images/icon/signin-signup/twitter.png" alt="image"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- signup-section end -->