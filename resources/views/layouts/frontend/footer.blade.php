  <!-- footer-section start -->
    <footer class="footer-section">
      <div class="footer-top">
        <div class="container">
          <div class="row justify-content-between">
            <div class="col-lg-3">
              <div class="footer-widget widget-about">
                <a href="/" class="footer-logo-text mb-20">Nepgeeks</a>
                <p>Tincidunt netus, adipiscinsihatra  amet enim arcu, esfermentuveli ultcel eu sem. Nec non et, quam non et eu rhoncus sed vesl varius libero nec tristique </p>
                <ul class="payment-metho-list">
                  <li><img src="{{asset('assets/images/icon/payment/payment.png')}}" alt="image"></li>
                </ul>
              </div>
            </div><!-- footer-widget end -->
            <div class="col-lg-8 mt-lg-0 mt-5">
              <div class="row mb-none-40">
                <div class="col-lg-12 mb-40">
                  <form class="footer-search-form mb-40">
                    <input type="email" name="footer-search_keyword" id="footer-search_keyword" placeholder="Search Keyword">
                    <button type="submit"><i class="flaticon-vr-glass"></i></button>
                  </form>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 mb-40">
                  <div class="footer-widget widget-short-links">
                    <h4 class="footer-title">Useful link</h4>
                    <ul class="footer-short-link-list">
                      <li><a href="#0">about</a></li>
                      <li><a href="#0">faq</a></li>
                      <li><a href="#0">contact</a></li>
                      <li><a href="#0">sing up</a></li>
                    </ul>
                  </div>
                </div><!-- footer-widget end -->
                <div class="col-lg-3 col-md-3 col-sm-6 mb-40">
                  <div class="footer-widget widget-short-links">
                    <h4 class="footer-title">Solution</h4>
                    <ul class="footer-short-link-list">
                      <li><a href="#0">Unique design</a></li>
                      <li><a href="#0">Mockup generator</a></li>
                      <li><a href="#0">24/7 support</a></li>
                      <li><a href="#0">Manual order</a></li>
                    </ul>
                  </div>
                </div><!-- footer-widget end -->
                <div class="col-lg-6 col-md-6 mb-40">
                  <div class="footer-widget widget-contact">
                    <h4 class="footer-title">Useful link</h4>
                    <ul class="footer-contact-list">
                      <li>Maecenas luctus neque, orci nam lortisn egestas, ac facilisis amet proin.</li>
                      <li>6803 Dickens Islands Apt. 567, PortMliew, TX 149420145</li>
                      <li>
                        <a href="mailto:ami@gmail.com">demo1234@gmail.com</a>
                        <a href="mailto:ami@gmail.com">demosupport@gmail.com</a>
                      </li>
                    </ul>
                  </div>
                </div><!-- footer-widget end -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container text-center border-top">
          <div class="copy-right-text">
            <p>Copyright © 2020 . All rights reserved</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- footer-section end -->