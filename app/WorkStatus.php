<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkStatus extends Model
{
    protected $fillable=['status_name','status_description'];
}
