<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssign extends Model
{
    protected $fillable=[
    'task_id','user_id','client_id','task_status','task_comments'
    ];
}
