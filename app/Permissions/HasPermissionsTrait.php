<?php 
namespace App\Permissions;

use App\Role;

trait HasPermissionsTrait {

  public function hasRole( ... $roles ) {

    foreach ($roles as $role) {

      if ($this->roles->contains('role_slug', $role)) {
        return true;
      }
    }
    return false;
  }

  public function roles() {

    return $this->belongsToMany(Role::class,'users_roles');

  }

}