<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['cat_id','type_id','product_code','product_name','product_image1','product_image2','product_image3','product_description','product_stock','product_type','product_printing','product_orientation'];
}
