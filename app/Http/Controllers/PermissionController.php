<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
	

	public function Permission()
	{ 

		//RoleTableSeeder.php
		$dev_role = new Role();
		$dev_role->role_slug = 'developer';
		$dev_role->role_name = 'Front-end Developer';
		$dev_role->save();

		$manager_role = new Role();
		$manager_role->role_slug = 'manager';
		$manager_role->role_name = 'Assistant Manager';
		$manager_role->save();

		$dev_role = Role::where('role_slug','developer')->first();
		$manager_role = Role::where('role_slug', 'manager')->first();

		$developer = new User();
		$developer->name = 'Ashish Dulal';
		$developer->email = 'admin@admin.com';
		$developer->image = 'default-image.png';
		$developer->password = bcrypt('secrettt');
		$developer->save();
		$developer->roles()->attach($dev_role);

		$manager = new User();
		$manager->name = 'Nakul Budhathoki';
		$manager->email = 'user@user.com';
		$manager->image = 'default-image.png';
		$manager->password = bcrypt('secrettt');
		$manager->save();
		$manager->roles()->attach($manager_role);

		
		return redirect()->back();
	}
}