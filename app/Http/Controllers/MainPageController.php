<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use App\ProductCategory;
use App\ProductVariation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function mainPage(){
    	return view('frontend.index');
    }

    public function aboutPage(){
    	return view('frontend.about');
    }

    public function productPage(){
        $productscoll = DB::table('products')
        ->leftjoin('product_types','products.type_id','=','product_types.id')
        ->leftjoin('product_categories','products.cat_id','=','product_categories.id')
        ->select('products.id','product_name','product_image1','product_image2','product_image3','product_categories.category_name')
        ->distinct()->get();
        $products = product::latest()->get();
        $productCat = ProductCategory::all();
        $productType = ProductType::all();

        return view('frontend.product',compact('products','productCat','productType'));
    }

    public function productDetailPage($id){
        $product = DB::table('products')
        ->leftjoin('product_types','products.type_id','=','product_types.id')
        ->leftjoin('product_categories','products.cat_id','=','product_categories.id')
        ->select('products.id','cat_id','type_id','product_code','product_name','product_image1','product_image2','product_image3','product_description','product_types.product_type','category_name','product_stock')
        ->where('products.id','=',$id)
        ->first();
        $productVar = ProductVariation::all();
        $firstVar = DB::table('product_variations')
        ->leftjoin('product_categories','product_variations.category_id','=','product_categories.id')
        ->get();
        $catId = product::where('id',$id)->first('cat_id');
        $otherProduct = DB::table('products')
        ->leftjoin('product_types','products.type_id','=','product_types.id')
        ->leftjoin('product_categories','products.cat_id','=','product_categories.id')
        ->select('products.id','product_name','product_image3','product_code','product_description')
        ->where('cat_id',$catId->cat_id)
        ->get();
        $productCat = ProductCategory::all();
        $productType = ProductType::all();
        return view('frontend.product-details',compact('product','productVar','firstVar','productCat','productType','otherProduct'));
    }
    public function variationSearch(Request $request){
        $variations = ProductVariation::where('id',$request->varId)->first();
        return $variations;
    }

    public function CategorySearch(Request $request){
        if($request->id == 'all'){
            $productCat = ProductCategory::all();
            $products = product::latest()->get();
            $showName = 0;
            return view('frontend.product-cat', compact('products', 'productCat','showName'));
        }
        elseif($request->id){
            $id = $request->id;
            $showName = 1;
            $productCatt = ProductCategory::all();
            $products = product::where('cat_id',$id)->latest()->get();
            $productCat = $productCatt->where('id',$id);
            return view('frontend.product-cat', compact('products', 'productCat','showName'));
        }
        elseif($request->type_id){
            $id = $request->type_id;
            $products = product::where('type_id',$id)->latest()->get();
            $productTypes = ProductType::all();
            $productType = $productTypes->where('id',$id);
            return view('frontend.product-type', compact('products', 'productType'));
        }
        else{
            $products = product::where('product_name','LIKE','%'.$request->search.'%')->get();
            return view('frontend.search', compact('products'));
        }
        // return response()->json(['success' => 'Successfully Filtered!', 'status' => 1]);
    }

    public function customDesignPage(){
    	return view('frontend.custom-design');
    }

    public function cartPage(){
    	return view('frontend.cart');
    }

    public function checkoutPage(){
    	return view('frontend.checkout');
    }

    public function privacyPage(){
    	return view('frontend.privacy');
    }

    public function faqPage(){
    	return view('frontend.faq');
    }

    public function blogsPage(){
    	return view('frontend.blogs');
    }

    public function blogDetailPage(){
    	return view('frontend.blog-detail');
    }

    public function contactPage(){
    	return view('frontend.contact');
    }
}