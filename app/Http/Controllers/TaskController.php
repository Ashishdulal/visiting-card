<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use App\WorkStatus;
use App\PaymentStatus;
use App\TaskAssign;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        $assignedTasks = DB::table('task_assigns')
        ->leftjoin('users','task_assigns.user_id','=','users.id')
        ->get();
        $userSelected = DB::table('task_assigns')
        ->leftjoin('users','task_assigns.client_id','=','users.id')
        ->get();
        $workStat = WorkStatus::all();
        $paymentStat = PaymentStatus::all();
        return view('backend.task.index',compact('tasks','workStat','paymentStat','assignedTasks','userSelected'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developers = DB::table('users_roles')
        ->leftjoin('users','users_roles.user_id','=','users.id')
        ->get();
        $userId = auth()->user()->id;
        $workStat = WorkStatus::all();
        $paymentStat = PaymentStatus::all();
        return view('backend.task.create',compact('workStat','paymentStat','userId','developers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $createdTask = Task::create($request->all());

     $taskAssign = TaskAssign::create([
        'task_id' => $createdTask->id,
        'user_id' => $request->user_id,
        'client_id' => $request->client_id,
        'task_status' => $request->work_status,
        'task_comments' => $request->comments
    ]);
     $developerMail = User::where('id',$taskAssign->user_id)->first('email');
    $data = array(
        'taskName'=> $createdTask->task_name,
        'payment'=> $createdTask->payment_status,
        'workStatus'=> $createdTask->work_status,
    );
    $txt1 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is Admin. </p>
    <p>This is to inform you that the following task has been assigned to you.</p>
    <p>Task Name: '.$data['taskName'].' </p>
    <p>Work Status:'. $data['workStatus'] .'</p>
    <p>It would be appriciative, if the work is completed in time.</p>
    <p>with Regards,<br>
        NTPL(Nepgeeks Technologies Pvt. Ltd,)<br>
        Putalisadak-32, kathmandu <br>
        Tel: 01-4011101, 9869178426, 9840465291
    </p>
    </body>
    </html>';      

    $to = $developerMail->email;
    $subject = "Customer Task Design";

    $headers = "From:design.nepgeeks.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt1,$headers);
    return $this->clientMail($taskAssign,$createdTask);
 }

public function clientMail($taskAssign,$createdTask){
    $clientMail = User::where('id',$taskAssign->client_id)->first('email');
    $datas = array(
        'taskName'=> $createdTask->task_name,
        'payment'=> $createdTask->payment_status,
        'workStatus'=> $createdTask->work_status,
    );
    $txt2 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is Nepgeeks Technology, Design Team. </p>
    <p>This is to inform you that the task - '.$datas['taskName'].' has been started.</p>
    <p>Payment Status :'. $datas['payment'] .'</p>
    <p>Work Status :'. $datas['workStatus'] .'</p>
    <p>This information will be repeted after the work is completed or for further status.</p>
    <p>with Regards,<br>
        NTPL(Nepgeeks Technologies Pvt. Ltd,)<br>
        Putalisadak-32, kathmandu <br>
        Tel: 01-4011101, 9869178426, 9840465291
    </p>
    </body>
    </html>';       
    $to = "$clientMail->email";
    $subject = "Design Task Status.";
    $headers = "From:design.nepgeeks.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
    $result=   mail($to,$subject,$txt2,$headers);
    return redirect('/home/tasks')->with('success','Task Created/Updated successfully And Mailed');
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task,$id)
    {
        $assignedTasks = DB::table('task_assigns')
        ->leftjoin('users','task_assigns.user_id','=','users.id')
        ->where('task_id',$id)
        ->get();
        $userSelected = DB::table('task_assigns')
        ->leftjoin('users','task_assigns.client_id','=','users.id')
        ->where('task_id',$id)
        ->get();
        $developers = DB::table('users_roles')
        ->leftjoin('users','users_roles.user_id','=','users.id')
        ->get();
        $task = Task::findOrFail($id);
        $workStat = WorkStatus::all();
        $paymentStat = PaymentStatus::all();
        return view('backend.task.edit',compact('workStat','paymentStat','task','developers','assignedTasks','userSelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task,$id)
    {
        $task = Task::findOrFail($id);
        $task->update($request->all());

        $aTaskId = TaskAssign::where('task_id',$id)->first();
        $taskAssign = $aTaskId->update([
        'task_id' => $id,
        'user_id' => $request->user_id,
        'client_id' => $request->client_id,
        'task_status' => $request->work_status,
        'task_comments' => $request->comments
    ]);
     $developerMail = User::where('id',$aTaskId->user_id)->first('email');
    $data = array(
        'taskName'=> $task->task_name,
        'payment'=> $task->payment_status,
        'workStatus'=> $task->work_status,
    );
    $txt1 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is Admin. </p>
    <p>This is to inform you that the following task has been assigned to you.</p>
    <p>Task Name: '.$data['taskName'].' </p>
    <p>Work Status:'. $data['workStatus'] .'</p>
    <p>It would be appriciative, if the work is completed in time.</p>
    <p>with Regards,<br>
        NTPL(Nepgeeks Technologies Pvt. Ltd,)<br>
        Putalisadak-32, kathmandu <br>
        Tel: 01-4011101, 9869178426, 9840465291
    </p>
    </body>
    </html>';      

    $to = $developerMail->email;
    $subject = "Customer Task Design";

    $headers = "From:design.nepgeeks.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt1,$headers);
    return $this->clientMail($taskAssign,$createdTask);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task,$id)
    {
        task::findOrFail($id)->delete();
        return redirect()->back()->with('success','Task deleted successfully');
    }
}
