<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use App\ProductVariation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pVar = ProductVariation::all();
        $allcat = ProductCategory::latest()->get();
        return view('backend.product.product-category.index',compact('allcat','pVar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.product.product-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catId = ProductCategory::create($request->all());

        $thicknessVariations= $request->product_thickness;
        $quantityVariations= $request->product_quantity;
        $priceVariations= $request->product_price;
        $sideVariations= $request->product_sides;
        if($thicknessVariations){
        foreach($thicknessVariations as $key=> $thick){

            ProductVariation::create([
                'category_id'=>$catId->id,
                'product_thickness'=>$thicknessVariations[$key],
                'product_quantity'=>$quantityVariations[$key],
                'product_price'=>$priceVariations[$key],
                'product_sides'=>$sideVariations[$key]
            ]);

        }
    }
        return redirect('/home/product-category')->with('success','Product Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory,$id)
    {
        $pVar = ProductVariation::all();
        $cat = ProductCategory::findOrFail($id);
        return view('backend.product.product-category.edit',compact('cat','pVar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory,$id)
    {
        $cat = ProductCategory::findOrFail($id);
        $cat->update($request->all());

        $productVar = ProductVariation::where('category_id',$id)->delete();

        $thicknessVariations= $request->product_thickness;
        $quantityVariations= $request->product_quantity;
        $priceVariations= $request->product_price;
        $sideVariations= $request->product_sides;
        if($thicknessVariations){
        foreach($thicknessVariations as $key=> $thick){

            ProductVariation::create([
                'category_id'=>$id,
                'product_thickness'=>$thicknessVariations[$key],
                'product_quantity'=>$quantityVariations[$key],
                'product_price'=>$priceVariations[$key],
                'product_sides'=>$sideVariations[$key]
            ]);

        }
    }

        return redirect('/home/product-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory,$id)
    {
        $cat = ProductCategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
