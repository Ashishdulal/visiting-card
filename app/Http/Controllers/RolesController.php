<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	$roles = Role::all();
    	return view ('backend.roles.index',compact('roles'));
    }
    public function create(){
    	return view('backend.roles.create');
    }
    public function store(Request $request){
        Role::create($request->all());
    	return redirect('/home/roles');
    }
    public function edit($id){
    	$role = Role::findOrFail($id);
    	return view('backend.roles.edit',compact('role'));
    }
    public function update(Request $request, $id){
        $input = $request->all();
        $role = Role::findOrFail($id);
        $role->update($input);
    	return redirect('/home/roles');
    }
    public function destroy($id)
    {
    	Role::findOrFail($id)->delete();
    	return redirect()->back();
    }
}
