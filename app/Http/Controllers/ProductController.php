<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')
        ->leftjoin('product_categories','products.cat_id','=','product_categories.id')
        ->leftjoin('product_types','products.type_id','=','product_types.id')
        ->select('products.id','product_name','product_stock','product_types.product_type','product_code','product_description','cat_id','type_id','product_image1','product_image2','product_image3','product_categories.category_name')
        ->get();
        return view('backend.product.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = ProductType::all();
        $products = Product::all();
        $pCat = ProductCategory::all();
        return view('backend.product.products.create',compact('products','pCat','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->cat_id = $request->cat_id;
        $product->type_id = $request->type_id;
        $product->product_code = $request->product_code;
        $product->product_name = $request->product_name;
        $product->product_description= $request->product_description;
        $product->product_stock= $request->product_stock;

        if(file_exists($request->file('product_image1'))){
            $image = $request->file('product_image1');
            $imageName =  "product".time().'.'.$request->file('product_image1')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image1 = $imageName;
        }
        else{
            $product->product_image1 = 'default-image.png';
        }
        if(file_exists($request->file('product_image2'))){
            $image = $request->file('product_image2');
            $imageName =  "product".time().'.'.$request->file('product_image2')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image2 = $imageName;
        }
        else{
            $product->product_image2 = 'default-image.png';
        }
        if(file_exists($request->file('product_image3'))){
            $image = $request->file('product_image3');
            $imageName =  "product".time().'.'.$request->file('product_image3')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image3 = $imageName;
        }
        else{
            $product->product_image3 = 'default-image.png';
        }
        $product->save();
        return redirect('/home/products')->with('success','Product Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product,$id)
    {
        $type = ProductType::all();
        $pCat = ProductCategory::latest()->get();
        $product = Product::findOrFail($id);
        return view('backend.product.products.edit',compact('product','pCat','type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product,$id)
    {
        $product = Product::findOrFail($id);
        $product->cat_id = $request->cat_id;
        $product->type_id = $request->type_id;
        $product->product_code = $request->product_code;
        $product->product_name = $request->product_name;
        $product->product_description= $request->product_description;
        $product->product_stock= $request->product_stock;

        if(file_exists($request->file('product_image1'))){
            $image = $request->file('product_image1');
            $imageName =  "product".time().'.'.$request->file('product_image1')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image1 = $imageName;
        }
        else{
            $product->product_image1 = $product->product_image1;
        }
        if(file_exists($request->file('product_image2'))){
            $image = $request->file('product_image2');
            $imageName =  "product".time().'.'.$request->file('product_image2')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image2 = $imageName;
        }
        else{
            $product->product_image2 = $product->product_image2;
        }
        if(file_exists($request->file('product_image3'))){
            $image = $request->file('product_image3');
            $imageName =  "product".time().'.'.$request->file('product_image3')->getClientOriginalName();
            $image->move(public_path('uploads/product'),$imageName);
            $product->product_image3 = $imageName;
        }
        else{
            $product->product_image3 = $product->product_image3;
        }
        $product->save();
        return redirect('/home/products')->with('success','Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product,$id)
    {
        Product::findOrFail($id)->delete();
        return redirect()->back()->with('success','Product Deleted Succesfully.');
    }
}
