<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class AllUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$users = User::latest()->get();
        $user = User::where('id', auth()->user()->id)->first();
    	$userRoles = DB::table('users_roles')
        ->leftjoin('roles','users_roles.role_id','=','roles.id')
        ->get();
        return view('backend.user.index',compact('users','userRoles','user'));
    }
    public function create(){
    	$roles = Role::latest()->get();
    	return view ('backend.user.create',compact('roles'));
    }
    public function userEdit($id){
        $user = User::where('id', auth()->user()->id)->first();
        $roles = Role::latest()->get();
        $selectedRoles = DB::table('users_roles')
        ->leftjoin('roles','users_roles.role_id','=','roles.id')
        ->where('user_id',$id)
        ->get();
        return view('backend.user.edit',compact('user','roles','selectedRoles'));
    }

    public function edit($id){
        $user = User::findOrFail($id);
        $roles = Role::latest()->get();
        $selectedRoles = DB::table('users_roles')
        ->leftjoin('roles','users_roles.role_id','=','roles.id')
        ->where('user_id',$id)
        ->get();
        return view('backend.user.edit',compact('user','roles','selectedRoles'));
    }
    public function update(Request $request, $id){
        $input = $request->all();
        $user = User::findOrFail($id);
        if(file_exists($request->file('image'))){
            $image = $request->file('image');
            $imageName =  "user".time().'.'.$request->file('image')->getClientOriginalName();
            $image->move(public_path('uploads/user'),$imageName);
            $userImage = $imageName;
        }
        else{
            $userImage = $user->image;
        }

        $user->update([
            'name'=> $request->name,
            'email'=> $request->email,
            'image'=> $userImage,
        ]   );
        DB::table('users_roles')
        ->where('user_id', $id)
        ->update([
            'role_id' => $request->role_id,
        ]);
        return redirect('/home/users');
    }
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return redirect()->back();
    }
    

}