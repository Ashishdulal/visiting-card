<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $fillable=[
    	'product_thickness','product_quantity','product_price','product_sides','category_id'
    ];
}
