<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('cat_id');
            $table->Integer('type_id');
            $table->string('product_code');
            $table->text('product_name');
            $table->text('product_image1');
            $table->text('product_image2')->nullable();
            $table->text('product_image3')->nullable();
            $table->text('product_description');
            $table->integer('product_stock')->default(0);
            $table->text('product_type')->nullable();
            $table->text('product_printing')->nullable();
            $table->text('product_orientation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
