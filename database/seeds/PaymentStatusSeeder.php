<?php

use Illuminate\Database\Seeder;
use App\PaymentStatus;
use Illuminate\Support\Facades\DB;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment = new PaymentStatus();
        $payment->payment_status_name = 'Partially Paid';
        $payment->save();
        $payment = new PaymentStatus();
        $payment->payment_status_name = 'Fully Paid';
        $payment->save();
        $payment = new PaymentStatus();
        $payment->payment_status_name = 'Not Paid';
        $payment->save();
    }
}
