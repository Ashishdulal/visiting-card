<?php

use App\WorkStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new WorkStatus();
        $status->status_name = 'Allocated';
        $status->save();
        $status = new WorkStatus;
        $status->status_name = 'On Progress';
        $status->save();
        $status = new WorkStatus;
        $status->status_name = 'On Review';
        $status->save();
        $status = new WorkStatus;
        $status->status_name = 'Editing';
        $status->save();
        $status = new WorkStatus;
        $status->status_name = 'Completed';
        $status->save();
    }
}
